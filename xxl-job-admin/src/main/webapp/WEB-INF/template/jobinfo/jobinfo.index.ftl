<!DOCTYPE html>
<html>
<head>
    <title>任务调度中心</title>
<#import "/common/common.macro.ftl" as netCommon>
<@netCommon.commonStyle />
    <!-- DataTables -->
    <link rel="stylesheet" href="${request.contextPath}/static/adminlte/plugins/datatables/dataTables.bootstrap.css">
    <!-- select2 -->
    <link href="${request.contextPath}/static/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
    <link href="${request.contextPath}/static/plugins/select2/select2-bootstrap.css" rel="stylesheet" type="text/css"/>
</head>
<body class="hold-transition skin-blue sidebar-mini <#if cookieMap?exists && "off" == cookieMap["xxljob_adminlte_settings"].value >sidebar-collapse</#if>">
<div class="wrapper">
    <!-- header -->
<@netCommon.commonHeader />
    <!-- left -->
<@netCommon.commonLeft "jobinfo" />

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>任务管理<small>任务调度中心</small></h1>
            <!--
            <ol class="breadcrumb">
                <li><a><i class="fa fa-dashboard"></i>调度管理</a></li>
                <li class="active">调度中心</li>
            </ol>
            -->
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-xs-2">
                    <div class="input-group">
                        <span class="input-group-addon">执行器</span>
                        <select class="form-control" id="jobGroup" >
                            <option value="0" >全部</option>
                        <#list JobGroupList as group>
                            <option value="${group.id}" <#if jobGroup==group.id>selected</#if> >${group.title}</option>
                        </#list>
                        </select>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="input-group">
                        <span class="input-group-addon">JobHandler</span>
                        <input type="text" class="form-control" id="executorHandler" autocomplete="on" >
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="input-group">
                        <span class="input-group-addon">调度名称</span>
                        <input type="text" class="form-control" id="executorName" autocomplete="on" >
                    </div>
                </div>
                <div class="col-xs-2">
                    <button class="btn btn-block btn-info" id="searchBtn">搜索</button>
                </div>
                <div class="col-xs-2">
                    <button class="btn btn-block btn-success add" type="button">+新增任务</button>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header hide">
                            <h3 class="box-title">调度列表</h3>
                        </div>
                        <div class="box-body" >
                            <table id="job_list" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th name="id" >id</th>
                                    <th name="glueSource"></th>
                                    <th name="jobGroup" >jobGroup</th>
                                    <th name="childJobKey" >JobKey</th>
                                    <th name="jobDesc" >描述</th>
                                    <th name="glueType" >运行模式</th>
                                    <th name="executorParam" >任务参数</th>
                                    <th name="jobCron" >Cron</th>
                                    <th name="addTime" >新增时间</th>
                                    <th name="updateTime" >更新时间</th>
                                    <th name="author" >负责人</th>
                                    <th name="alarmEmail" >报警邮件</th>
                                    <th name="glueType" >运行模式</th>
                                    <th name="jobStatus" >状态</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot></tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- footer -->
<@netCommon.commonFooter />
</div>

<!-- job新增.模态框 -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" >新增任务</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal form" role="form" >
                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label">执行器<font color="red">*</font></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="jobGroup" >
                            <#list JobGroupList as group>
                                <option value="${group.id}" <#if jobGroup==group.id>selected</#if> >${group.title}</option>
                            </#list>
                            </select>
                        </div>
                        <label for="lastname" class="col-sm-2 control-label">任务描述<font color="red">*</font></label>
                        <div class="col-sm-4"><input type="text" class="form-control" name="jobDesc" placeholder="请输入“描述”" maxlength="50" ></div>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label">路由策略<font color="red">*</font></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="executorRouteStrategy" >
                            <#list ExecutorRouteStrategyEnum as item>
                                <option value="${item}" >${item.title}</option>
                            </#list>
                            </select>
                        </div>
                        <label for="lastname" class="col-sm-2 control-label">Cron<font color="red">*</font></label>
                        <div class="col-sm-4"><input type="text" class="form-control" name="jobCron" placeholder="请输入“Cron”" maxlength="20" ></div>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label">运行模式<font color="red">*</font></label>
                        <div class="col-sm-4">
                            <select class="form-control glueType" name="glueType" >
                            <#list GlueTypeEnum as item>
                                <option value="${item}" >${item.desc}</option>
                            </#list>
                            </select>
                        </div>
                        <label for="firstname" class="col-sm-2 control-label">JobHandler<font color="black">*</font></label>
                        <div class="col-sm-4"><input type="text" class="form-control" name="executorHandler" placeholder="请输入“JobHandler”" maxlength="100" ></div>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label">阻塞处理策略<font color="red">*</font></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="executorBlockStrategy" >
                            <#list ExecutorBlockStrategyEnum as item>
                                <option value="${item}" >${item.title}</option>
                            </#list>
                            </select>
                        </div>
                        <label for="lastname" class="col-sm-2 control-label">失败处理策略<font color="red">*</font></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="executorFailStrategy" >
                            <#list ExecutorFailStrategyEnum as item>
                                <option value="${item}" >${item.title}</option>
                            </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="col-sm-2 control-label">负责人<font color="red">*</font></label>
                        <div class="col-sm-4"><input type="text" class="form-control" name="author" placeholder="请输入“负责人”" maxlength="50" ></div>
                        <label for="lastname" class="col-sm-2 control-label">报警邮件<font color="black">*</font></label>
                        <div class="col-sm-4"><input type="text" class="form-control" name="alarmEmail" placeholder="请输入“报警邮件”，多个邮件地址逗号分隔" maxlength="100" ></div>
                    </div>

                    <div class="form-group">
                        <label for="lastname" class="col-sm-2 control-label">子任务Key<font color="black">*</font></label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="childJobKey" placeholder="请输入子任务的任务Key,如存在多个逗号分隔" maxlength="100" ></div>
                    </div>

                    <div id="executeTableId" class="form-group" style="display: none;">
                        <label for="executeTable" class="col-sm-2 control-label">执行目标表<font color="black">*</font></label>
                        <div class="col-sm-10"><input id="executeTableKeyId" type="text" class="form-control select2"/></div>
                    </div>

                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label">执行参数<font color="black">*</font></label>
                        <div class="col-sm-8">
                            <textarea class="textarea form-control" id="executorParamId" name="executorParam" placeholder="请输入“执行参数”" maxlength="512" style="height: 34px; line-height: 1.2;"></textarea>
                        </div>
                        <div class="col-sm-2" > <button class="btn btn-block btn-info addKettleParams" type="button">添加参数</button> </div>
                    </div>
                    <div id="procParamLists" class="form-group" style="display: none;padding-left: 30px;padding-right: 20px" >
                        <table  class="table table-bordered table-striped" >
                            <thead >
                            <tr>
                                <th>编号</th>
                                <th>参数值类型</th>
                                <th>参数类型</th>
                                <th>参数名称</th>
                                <th>参数值</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="addProcParamLists" >

                            </tbody>
                        </table>
                    </div>
                    <div class="form-group"  style="display: none;padding-left: 60px;padding-top: 10px;padding-right: 20px"  id="addKettleParams">
                        <table  class="table table-bordered table-striped" >
                            <thead >
                            <tr>
                                <th>参数名称</th>
                                <th>参数值</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="addKettleParamLists" >

                            </tbody>
                        </table>

                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button type="submit" class="btn btn-primary"  >保存</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        </div>
                    </div>

                    <input type="hidden" name="glueRemark" value="GLUE代码初始化" >
                    <textarea name="glueSource" style="display:none;" ></textarea>
                    <textarea class="glueSource_java" style="display:none;" >
package com.xxl.job.service.handler;

import com.xxl.job.core.log.XxlJobLogger;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;

public class DemoGlueJobHandler extends IJobHandler {

	@Override
	public ReturnT<String> execute(String... params) throws Exception {
		XxlJobLogger.log("XXL-JOB, Hello World.");
		return ReturnT.SUCCESS;
	}

}
</textarea>
                    <textarea class="glueSource_shell" style="display:none;" >
#!/bin/bash
echo "xxl-job: hello shell"

echo "脚本位置：$0"
echo "参数数量：$#"
for param in $*
do
    echo "参数 : $param"
    sleep 1s
done

echo "Good bye!"
exit 0
</textarea>
                    <textarea class="glueSource_python" style="display:none;" >
#!/usr/bin/python
# -*- coding: UTF-8 -*-
import time
import sys

print "xxl-job: hello python"
print "脚本文件：", sys.argv[0]
for i in range(1, len(sys.argv)):
	time.sleep(1)
	print "参数", i, sys.argv[i]

print "Good bye!"
exit(0)<#--
import logging
logging.basicConfig(level=logging.DEBUG)
logging.info("脚本文件：" + sys.argv[0])
-->
</textarea>
                    <textarea class="glueSource_proc" style="display:none;">
[
    {
	"index":1,
	"name":"xxx",
	"paramType":"IN",
	"valueType":"VARCHAR",
	"value":""
    },
    {
	"index":2,
	"name":"xxx",
	"paramType":"OUT",
	"valueType":"CURSOR"
    },
    {
	"index":3,
	"name":"xxx",
	"paramType":"OUT",
	"valueType":"VARCHAR"
    }
]
</textarea>
                    <textarea class="glueSource_kettle" style="display:none;">
	[
		{
			"name":"xxx",
			"value":""
		},
		{
			"name":"xxx",
			"value":"CURSOR"
		}
	]
</textarea>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- 更新.模态框 -->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" >更新任务</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal form" role="form" >
                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label">执行器<font color="red">*</font></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="jobGroup" disabled >
                            <#list JobGroupList as group>
                                <option value="${group.id}" >${group.title}</option>
                            </#list>
                            </select>
                        </div>
                        <label for="lastname" class="col-sm-2 control-label">任务描述<font color="red">*</font></label>
                        <div class="col-sm-4"><input type="text" class="form-control" name="jobDesc" placeholder="请输入“描述”" maxlength="50" ></div>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label">路由策略<font color="red">*</font></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="executorRouteStrategy" >
                            <#list ExecutorRouteStrategyEnum as item>
                                <option value="${item}" >${item.title}</option>
                            </#list>
                            </select>
                        </div>
                        <label for="lastname" class="col-sm-2 control-label">Cron<font color="red">*</font></label>
                        <div class="col-sm-4"><input type="text" class="form-control" name="jobCron" placeholder="请输入“Cron”" maxlength="20" ></div>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label">运行模式<font color="red">*</font></label>
                        <div class="col-sm-4">
                            <select class="form-control glueType" name="glueType" disabled >
                            <#list GlueTypeEnum as item>
                                <option value="${item}" >${item.desc}</option>
                            </#list>
                            </select>
                        </div>
                        <label for="firstname" class="col-sm-2 control-label">JobHandler<font color="black">*</font></label>
                        <div class="col-sm-4"><input type="text" class="form-control" name="executorHandler" placeholder="请输入“JobHandler”" maxlength="100" ></div>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label">阻塞处理策略<font color="red">*</font></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="executorBlockStrategy" >
                            <#list ExecutorBlockStrategyEnum as item>
                                <option value="${item}" >${item.title}</option>
                            </#list>
                            </select>
                        </div>
                        <label for="lastname" class="col-sm-2 control-label">失败处理策略<font color="red">*</font></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="executorFailStrategy" >
                            <#list ExecutorFailStrategyEnum as item>
                                <option value="${item}" >${item.title}</option>
                            </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="col-sm-2 control-label">负责人<font color="red">*</font></label>
                        <div class="col-sm-4"><input type="text" class="form-control" name="author" placeholder="请输入“负责人”" maxlength="50" ></div>
                        <label for="lastname" class="col-sm-2 control-label">报警邮件<font color="black">*</font></label>
                        <div class="col-sm-4"><input type="text" class="form-control" name="alarmEmail" placeholder="请输入“报警邮件”，多个邮件地址逗号分隔" maxlength="100" ></div>
                    </div>

                    <div class="form-group">
                        <label for="lastname" class="col-sm-2 control-label">子任务Key<font color="black">*</font></label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="childJobKey" placeholder="请输入子任务的任务Key,如存在多个逗号分隔" maxlength="100"></div>
                    </div>

                    <div id="executeTableUpdateId" class="form-group" style="display: none;">
                        <label for="executeTableUpdate" class="col-sm-2 control-label">执行目标表<font color="black">*</font></label>
                        <div class="col-sm-10"><input id="executeTableUpdateKeyId" type="text" class="form-control select2"/></div>

                    </div>

                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label">执行参数<font color="black">*</font></label>
                        <div class="col-sm-8">
                            <textarea class="textarea form-control" id="executorParamUpdateId" name="executorParam" placeholder="请输入“执行参数" maxlength="512" style="height: 34px; line-height: 1.2;"></textarea>
                        </div>
                        <div class="col-sm-2" > <button class="btn btn-block btn-info addUpdateKettleParams" style="display: none;" type="button">添加参数</button> </div>
                    </div>
                    <div id="procParamLists" class="form-group"  style="display: none;padding-left: 30px;padding-right: 20px;"  >

                        <table  class="table table-bordered table-striped" >
                            <thead >
                            <tr>
                                <th>编号</th>
                                <th>参数值类型</th>
                                <th>参数类型</th>
                                <th>参数名称</th>
                                <th>参数值</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="updateProcParamLists" >

                            </tbody>
                        </table>
                    </div>

                    <div class="form-group col-sm-12"  style="display: none;padding-left: 60px;padding-top: 10px;padding-right: 20px" id="updateKettleParams" >
                        <table  class="table table-bordered table-striped " >
                            <thead >
                            <tr>
                                <th>参数名称</th>
                                <th>参数值</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="updateKettleParamLists" >

                            </tbody>

                        </table>

                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button type="submit" class="btn btn-primary"  >保存</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                            <input type="hidden" name="id" >
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<@netCommon.commonScript />
<!-- DataTables -->
<script src="${request.contextPath}/static/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="${request.contextPath}/static/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="${request.contextPath}/static/plugins/jquery/jquery.validate.min.js"></script>
<!-- moment -->
<script src="${request.contextPath}/static/adminlte/plugins/daterangepicker/moment.min.js"></script>
<script src="${request.contextPath}/static/js/jobinfo.index.1.js"></script>
<!-- select2 -->
<script src="${request.contextPath}/static/plugins/select2/select2.min.js" type="text/javascript"></script>
<script src="${request.contextPath}/static/plugins/select2/select2_locale_zh-CN.js" type="text/javascript"></script>
</body>
</html>
