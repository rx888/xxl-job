$(function() {
    // init date tables
    initExecutorNameSelect2("executorName","executorName",base_url + "/joblog/select2JobName?kyTools.getUniqueID()",'请选择...');
    var jobTable = $("#job_list").dataTable({
        "deferRender": true,
        "processing" : true,
        "serverSide": true,
        "ajax": {
            url: base_url + "/jobinfo/pageList",
            type:"post",
            data : function ( d ) {
                var obj = {};
                obj.jobGroup = $('#jobGroup').val();
                obj.executorHandler = $('#executorHandler').val();
                obj.executorName = $('#executorName').select2('data') == null ? null : $('#executorName').select2('data').text;
                obj.start = d.start;
                obj.length = d.length;
                return obj;
            },
            error:function(result){
                layer.open({
                    title: '系统提示',
                    content: ("请求失败"),
                    icon: '2'
                });
            }
        },
        "searching": false,
        "ordering": false,
        //"scrollX": true,	// X轴滚动条，取消自适应
        "columns": [
            { "data": 'id', "bSortable": false, "visible" : false},
            { "data": 'glueSource', "bSortable": false, "visible" : false}  ,
            {
                "data": 'jobGroup',
                "visible" : false,
                "render": function ( data, type, row ) {
                    var groupMenu = $("#jobGroup").find("option");
                    for ( var index in $("#jobGroup").find("option")) {
                        if ($(groupMenu[index]).attr('value') == data) {
                            return $(groupMenu[index]).html();
                        }
                    }
                    return data;
                }
            },
            {
                "data": 'childJobKey',
                "width":'10%',
                "visible" : true,
                "render": function ( data, type, row ) {
                    var jobKey = row.jobGroup + "_" + row.id;
                    return jobKey;
                }
            },
            { "data": 'jobDesc', "visible" : true,"width":'20%'},
            {
                "data": 'glueType',
                "width":'20%',
                "visible" : true,
                "render": function ( data, type, row ) {
                    if ('GLUE_KETTLE' == row.glueType) {
                        return "GLUE模式(Kettle)";
                    } else if ('GLUE_PROC' == row.glueType) {
                        return "GLUE模式(Procedure)";
                    } else if ('GLUE_GROOVY'==row.glueType) {
                        return "GLUE模式(Java)";
                    } else if ('GLUE_SHELL'==row.glueType) {
                        return "GLUE模式(Shell)";
                    } else if ('GLUE_PYTHON'==row.glueType) {
                        return "GLUE模式(Python)";
                    } else if ('BEAN'==row.glueType) {
                        return "BEAN模式：" + row.executorHandler;
                    }
                    return row.executorHandler;
                }
            },
            { "data": 'executorParam', "visible" : false},
            { "data": 'jobCron', "visible" : true,"width":'10%'},
            {
                "data": 'addTime',
                "visible" : false,
                "render": function ( data, type, row ) {
                    return data?moment(new Date(data)).format("YYYY-MM-DD HH:mm:ss"):"";
                }
            },
            {
                "data": 'updateTime',
                "visible" : false,
                "render": function ( data, type, row ) {
                    return data?moment(new Date(data)).format("YYYY-MM-DD HH:mm:ss"):"";
                }
            },
            { "data": 'author', "visible" : true, "width":'10%'},
            { "data": 'alarmEmail', "visible" : false},
            { "data": 'glueType', "visible" : false},
            {
                "data": 'jobStatus',
                "width":'10%',
                "visible" : true,
                "render": function ( data, type, row ) {
                    if ('NORMAL' == data) {
                        return '<small class="label label-success" ><i class="fa fa-clock-o"></i>'+ data +'</small>';
                    } else if ('PAUSED' == data){
                        return '<small class="label label-default" title="暂停" ><i class="fa fa-clock-o"></i>'+ data +'</small>';
                    } else if ('BLOCKED' == data){
                        return '<small class="label label-default" title="阻塞[串行]" ><i class="fa fa-clock-o"></i>'+ data +'</small>';
                    }
                    return data;
                }
            },
            {
                "data": '操作' ,
                "width":'15%',
                "render": function ( data, type, row ) {
                    return function(){
                        // status
                        var pause_resume = "";
                        if ('NORMAL' == row.jobStatus) {
                            pause_resume = '<button class="btn btn-primary btn-xs job_operate" _type="job_pause" type="button">暂停</button>  ';
                        } else if ('PAUSED' == row.jobStatus){
                            pause_resume = '<button class="btn btn-primary btn-xs job_operate" _type="job_resume" type="button">恢复</button>  ';
                        }
                        // log url
                        var logUrl = base_url +'/joblog?jobId='+ row.id;

                        // log url
                        var codeBtn = "";
                        if ('BEAN' != row.glueType) {
                            var codeUrl = base_url +'/jobcode?jobId='+ row.id;
                            codeBtn = '<button class="btn btn-warning btn-xs" type="button" onclick="javascript:window.open(\'' + codeUrl + '\')" >GLUE</button>  '
                        }

                        // html
                        tableData['key'+row.id] = row;
                        var html = '<p id="'+ row.id +'" >'+
                            '<button class="btn btn-primary btn-xs job_operate" _type="job_trigger" type="button">执行</button>  '+
                            pause_resume +
                            '<button class="btn btn-primary btn-xs" type="job_del" type="button" onclick="javascript:window.open(\'' + logUrl + '\')" >日志</button><br>  '+
                            '<button class="btn btn-warning btn-xs update" type="button">编辑</button>  '+
                            codeBtn +
                            '<button class="btn btn-danger btn-xs job_operate" _type="job_del" type="button">删除</button>  '+
                            '</p>';

                        return html;
                    };
                }
            }
        ],
        "language" : {
            "sProcessing" : "处理中...",
            "sLengthMenu" : "每页 _MENU_ 条记录",
            "sZeroRecords" : "没有匹配结果",
            "sInfo" : "第 _PAGE_ 页 ( 总共 _PAGES_ 页，_TOTAL_ 条记录 )",
            "sInfoEmpty" : "无记录",
            "sInfoFiltered" : "(由 _MAX_ 项结果过滤)",
            "sInfoPostFix" : "",
            "sSearch" : "搜索:",
            "sUrl" : "",
            "sEmptyTable" : "表中数据为空",
            "sLoadingRecords" : "载入中...",
            "sInfoThousands" : ",",
            "oPaginate" : {
                "sFirst" : "首页",
                "sPrevious" : "上页",
                "sNext" : "下页",
                "sLast" : "末页"
            },
            "oAria" : {
                "sSortAscending" : ": 以升序排列此列",
                "sSortDescending" : ": 以降序排列此列"
            }
        }
    });

    // table data
    var tableData = {};

    // 搜索按钮
    $('#searchBtn').on('click', function(){
        jobTable.fnDraw();
    });

    // jobGroup change
    $('#jobGroup').on('change', function(){
        //reload
        var jobGroup = $('#jobGroup').val();
        window.location.href = base_url + "/jobinfo?jobGroup=" + jobGroup;
    });

    // job operate
    $("#job_list").on('click', '.job_operate',function() {
        var typeName;
        var url;
        var needFresh = false;

        var type = $(this).attr("_type");
        if ("job_pause" == type) {
            typeName = "暂停";
            url = base_url + "/jobinfo/pause";
            needFresh = true;
        } else if ("job_resume" == type) {
            typeName = "恢复";
            url = base_url + "/jobinfo/resume";
            needFresh = true;
        } else if ("job_del" == type) {
            typeName = "删除";
            url = base_url + "/jobinfo/remove";
            needFresh = true;
        } else if ("job_trigger" == type) {
            typeName = "执行";
            url = base_url + "/jobinfo/trigger";
        } else {
            return;
        }

        var id = $(this).parent('p').attr("id");

        layer.confirm('确认' + typeName + '?', {icon: 3, title:'系统提示'}, function(index){
            layer.close(index);

            $.ajax({
                type : 'POST',
                url : url,
                data : {
                    "id" : id
                },
                dataType : "json",
                success : function(data){
                    if (data.code == 200) {

                        layer.open({
                            title: '系统提示',
                            content: typeName + "成功",
                            icon: '1',
                            end: function(layero, index){
                                if (needFresh) {
                                    //window.location.reload();
                                    jobTable.fnDraw();
                                }
                            }
                        });
                    } else {
                        layer.open({
                            title: '系统提示',
                            content: (data.msg || typeName + "失败"),
                            icon: '2'
                        });
                    }
                },
            });
        });
    });

    // jquery.validate 自定义校验 “英文字母开头，只含有英文字母、数字和下划线”
    jQuery.validator.addMethod("myValid01", function(value, element) {
        var length = value.length;
        var valid = /^[a-zA-Z][a-zA-Z0-9_]*$/;
        return this.optional(element) || valid.test(value);
    }, "只支持英文字母开头，只含有英文字母、数字和下划线");

    // 新增
    $(".add").click(function(){
        executeTableKeySelect2("executeTableKeyId","executorParamId",base_url + "/jobinfo/executeTableKeySelect2?kyTools.getUniqueID()",'请选择...');
        $("#executeTableId").hide();
        $(".addKettleParams").hide();
        $('#addModal').modal({backdrop: false, keyboard: false}).modal('show');
    });
    var addModalValidate = $("#addModal .form").validate({
        errorElement : 'span',
        errorClass : 'help-block',
        focusInvalid : true,
        rules : {
            jobDesc : {
                required : true,
                maxlength: 50
            },
            jobCron : {
                required : true
            },
            author : {
                required : true
            },
            executorParam:{
                "checkExecutorParamValue":true
            }
        },
        messages : {
            jobDesc : {
                required :"请输入“描述”."
            },
            jobCron : {
                required :"请输入“Cron”."
            },
            author : {
                required : "请输入“负责人”."
            },
            executorParam:{
                "checkExecutorParamValue": '该运行模式下“执行参数”必填'
            }
        },
        highlight : function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        success : function(label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement : function(error, element) {
            element.parent('div').append(error);
        },
        submitHandler : function(form) {
            if (checkAddKettle()) {
                if(checkAddProc()){
                    // post
                    $.post(base_url + "/jobinfo/add",  $("#addModal .form").serialize(), function(data, status) {
                        if (data.code == "200") {
                            $('#addModal').modal('hide');
                            layer.open({
                                title: '系统提示',
                                content: '新增任务成功',
                                icon: '1',
                                end: function(layero, index){
                                    jobTable.fnDraw();
                                    //window.location.reload();
                                }
                            });
                        } else {
                            layer.open({
                                title: '系统提示',
                                content: (data.msg || "新增失败"),
                                icon: '2'
                            });
                        }
                    });
                }else{
                    layer.open({
                        title: '系统提示',
                        content: ("PROC模式的参数名称必填，并且执行参数“?”号数量需等于实际数量！"),
                        icon: '2'
                    });
                }
            }else{
                layer.open({
                    title: '系统提示',
                    content: ("KETTLE 模式的参数名称必填"),
                    icon: '2'
                });
            }
        }
    });

    $.validator.addMethod('checkExecutorParamValue', function(value, element, param){
        var _result = true;
        if(value=='' && ($("#addModal .glueType").val()=='GLUE_PROC'||$("#addModal .glueType").val()=='GLUE_KETTLE')){
            return false;
        }
        if(value=='' && ($("#updateModal .glueType").val()=='GLUE_PROC'||$("#updateModal .glueType").val()=='GLUE_KETTLE')){
            return false;
        }
        return _result;
    });



    $("#addModal").on('hide.bs.modal', function () {
        $("#addModal .form")[0].reset();
        addModalValidate.resetForm();
        $("#addModal .form .form-group").removeClass("has-error");
        $(".remote_panel").show();	// remote
        $("#addModal").find("div[type='procParam']").remove();
        $("#addModal #procParamLists").hide();
        //$("[type='kettleParamLists']").remove();
        $("#addKettleParams").hide();
        $("#addModal .form input[name='executorHandler']").removeAttr("readonly");
        $("#addProcParamLists").empty();
        $("#addKettleParamLists").empty();
    });


    // 运行模式
    $(".glueType").change(function(){
        // executorHandler
        var $executorHandler = $(this).parents("form").find("input[name='executorHandler']");
        var glueType = $(this).val();
        $("#addProcParamLists").empty();
        $("#addKettleParamLists").empty();

        if ('BEAN' == glueType) {
            $executorHandler.removeAttr("readonly");
            $("#executeTableId").hide();
        } else if('GLUE_KETTLE' == glueType){
            $executorHandler.val("");
            $executorHandler.attr("readonly","readonly");
            $("#executeTableId").show();
            $("#executeTableKeyId").select2("data",null);
            $(".addKettleParams").show();
        } else {
            $executorHandler.val("");
            $executorHandler.attr("readonly","readonly");
            $("#executeTableId").hide();
            $("#addKettleParams").hide();
            $(".addKettleParams").hide();
        }
        if(glueType=='GLUE_PROC'){
            $("#addModal #procParamLists").show();
            addModalBuildPorcInfo();
        }else{
            $("#addModal #procParamLists").hide();
        }
    });
    //执行目标表选择
    var executeTableKeySelect2 = function(prodId,valId,url,placeholder){
        var $prod = $("#" + prodId);
        $prod.select2({
            ajax: {
                type:'GET',
                url: url,
                dataType: 'json',
                data: function (term, page) {
                    return {
                        vcName: term
                    };
                },
                results: function (data, params) {
                    var d = {results: []};
                    $.each(data, function(i, item) {
                        d.results.push({id: item.etlJobAuto, text: item.tabName,jobId:item.jobId});
                    });
                    return d;
                },
                cache: false
            },
            placeholder:placeholder,
            language: "zh-CN",
            //tags: true,
            allowClear: true,
            escapeMarkup: function (markup) { return markup; },
            formatResult: function formatRepo(repo){return repo.text;}, // 函数用来渲染结果
            formatSelection: function formatRepoSelection(repo){
                $("#"+valId).val(repo.id);
                if(repo.jobId !=undefined){
                    if(valId=='executorParamId'){
                        $("#addKettleParams").show();
                        $("#addKettleParamLists").empty().prepend("<tr type='addKettleParamLists'>" +
                            "<td> "+
                            "   <input type='text' class='form-control' name='xxlJobKettleParamsList[0].name' value='job'  placeholder='请输入“参数名称”' maxlength='100' >" +
                            "</td>"+
                            " <td>" +
                            "    <input type='text' class='form-control' name='xxlJobKettleParamsList[0].value' value='"+repo.jobId +"'  placeholder='请输入“参数值”'  maxlength='100' >" +
                            "       </td>" +
                            "       <td>" +
                            "      <div><button class='btn btn-danger btn-xs' _type='kettleParams_del' type='button'>删除</button> </div>" +
                            "    </td>" +
                            " </tr>"+
                            "<tr type='addKettleParamLists'>" +
                            "<td> "+
                            "   <input type='text' class='form-control' name='xxlJobKettleParamsList[1].name' value='start_date'  placeholder='请输入“参数名称”' maxlength='100' >" +
                            "</td>"+
                            " <td>" +
                            "    <input type='text' class='form-control' name='xxlJobKettleParamsList[1].value' value=''  placeholder='请输入“参数值”'  maxlength='100' >" +
                            "       </td>" +
                            "       <td>" +
                            "      <div><button class='btn btn-danger btn-xs' _type='kettleParams_del' type='button'>删除</button> </div>" +
                            "    </td>" +
                            " </tr>"+
                            "<tr type='addKettleParamLists'>" +
                            "<td> "+
                            "   <input type='text' class='form-control' name='xxlJobKettleParamsList[2].name' value='end_date'  placeholder='请输入“参数名称”'  maxlength='100' >" +
                            "</td>"+
                            " <td>" +
                            "    <input type='text' class='form-control' name='xxlJobKettleParamsList[2].value' value=''  placeholder='请输入“参数值”' maxlength='100' >" +
                            "       </td>" +
                            "       <td>" +
                            "      <div><button class='btn btn-danger btn-xs' _type='kettleParams_del' type='button'>删除</button> </div>" +
                            "    </td>" +
                            " </tr>");
                        $("[_type='kettleParams_del']").on("click",function() {
                            var this_name =$(this).parents("td").prev().prev().find("input").attr("name");
                            var this_value =$(this).parents("td").prev().find("input").attr("name");


                            $(this).parents("tr").nextAll().each(function(){
                                var nextName = $(this).find("[name$='.name']").attr("name") ;
                                var nextValue = $(this).find("[name$='.value']").attr("name") ;
                                $(this).find("[name$='.name']").attr("name",this_name);
                                $(this).find("[name$='.value']").attr("name",this_value);
                                this_name = nextName;
                                this_value =nextValue;
                            })
                            $(this).parents("tr").remove();

                        })
                    }else{
                        $("#updateKettleParams").show();
                        $("#updateKettleParamLists").empty().prepend("<tr type='updateKettleParamLists'>" +
                            "<td> "+
                            "   <input type='text' class='form-control'  name='xxlJobKettleParamsList[0].name' value='job'  placeholder='请输入“参数名称”' maxlength='100' >" +
                            "</td>"+
                            " <td>" +
                            "    <input type='text' class='form-control' name='xxlJobKettleParamsList[0].value' value='"+repo.jobId +"'  placeholder='请输入“参数值”' maxlength='100' >" +
                            "       </td>" +
                            "       <td>" +
                            "      <div><button class='btn btn-danger btn-xs' _type='kettleParams_del' type='button'>删除</button> </div>" +
                            "    </td>" +
                            " </tr>"+
                            "<tr type='updateKettleParamLists'>" +
                            "<td> "+
                            "   <input type='text' class='form-control'  name='xxlJobKettleParamsList[1].name' value='start_date'  placeholder='请输入“参数名称”' maxlength='100' >" +
                            "</td>"+
                            " <td>" +
                            "    <input type='text' class='form-control dropup' name='xxlJobKettleParamsList[1].value' value=''  placeholder='请输入“参数值”' maxlength='100' >" +
                            "       </td>" +
                            "       <td>" +
                            "      <div><button class='btn btn-danger btn-xs' _type='kettleParams_del' type='button'>删除</button> </div>" +
                            "    </td>" +
                            " </tr>"+
                            "<tr type='updateKettleParamLists'>" +
                            "<td> "+
                            "   <input type='text' class='form-control'  name='xxlJobKettleParamsList[2].name' value='end_date'  placeholder='请输入“参数名称”' maxlength='100' >" +
                            "</td>"+
                            " <td>" +
                            "    <input type='text' class='form-control' name='xxlJobKettleParamsList[2].value' value=''  placeholder='请输入“参数值”' maxlength='100' >" +
                            "       </td>" +
                            "       <td>" +
                            "      <div><button class='btn btn-danger btn-xs' _type='kettleParams_del' type='button'>删除</button> </div>" +
                            "    </td>" +
                            " </tr>");
                        $("[_type='kettleParams_del']").on("click",function() {
                            var this_name =$(this).parents("td").prev().prev().find("input").attr("name");
                            var this_value =$(this).parents("td").prev().find("input").attr("name");


                            $(this).parents("tr").nextAll().each(function(){
                                var nextName = $(this).find("[name$='.name']").attr("name") ;
                                var nextValue = $(this).find("[name$='.value']").attr("name") ;
                                $(this).find("[name$='.name']").attr("name",this_name);
                                $(this).find("[name$='.value']").attr("name",this_value);
                                this_name = nextName;
                                this_value =nextValue;
                            })
                            $(this).parents("tr").remove();

                        })
                    }
                }

                return repo.text;
            } // 函数用于呈现当前的选择
        });
    };
    //关闭事件
    $("#executeTableKeyId").on("select2-clearing", function (e) {
        $("#executorParamId").val("");
        $("#addKettleParamLists").empty();
    });
    $("#executeTableUpdateKeyId").on("select2-clearing", function (e) {
        $("#executorParamUpdateId").val("");
        $("#updateKettleParamLists").empty();
    });

    $("#addModal .glueType").change(function(){
        // glueSource
        var glueType = $(this).val();
        if ('GLUE_GROOVY'==glueType){
            $("#addModal .form textarea[name='glueSource']").val( $("#addModal .form .glueSource_java").val() );
        } else if ('GLUE_SHELL'==glueType){
            $("#addModal .form textarea[name='glueSource']").val( $("#addModal .form .glueSource_shell").val() );
        } else if ('GLUE_PYTHON'==glueType){
            $("#addModal .form textarea[name='glueSource']").val( $("#addModal .form .glueSource_python").val() );
        } else if ('GLUE_PROC'==glueType){
            $("#addModal .form textarea[name='glueSource']").val( $("#addModal .form .glueSource_proc").val() );
        }else if ('GLUE_KETTLE'==glueType){
            $("#addModal .form textarea[name='glueSource']").val( $("#addModal .form .glueSource_kettle").val() );
        }
    });

    // 更新
    $("#job_list").on('click', '.update',function() {
        $("#updateKettleParamLists").empty();
        $("#updateProcParamLists").empty();
        $("#updateKettleParams").hide();
        executeTableKeySelect2("executeTableUpdateKeyId","executorParamUpdateId",base_url + "/jobinfo/executeTableKeySelect2?kyTools.getUniqueID()",'请选择...');
        var id = $(this).parent('p').attr("id");
        var row = tableData['key'+id];
        if (!row) {
            layer.open({
                title: '系统提示',
                content: ("任务信息加载失败，请刷新页面"),
                icon: '2'
            });
            return;
        }
        if ("GLUE_KETTLE"==row.glueType) {
            $(".addUpdateKettleParams").show();
            $("#executeTableUpdateId").show();
            try{
                var kettleParamLists = JSON.parse(row.glueSource);
                for(var i =kettleParamLists.length-1;i>=0;i--){
                    $("#updateKettleParams").show();
                    $("#updateKettleParamLists").prepend("<tr type='updateKettleParamLists'>" +
                        "<td> "+
                        "   <input type='text' class='form-control' name='xxlJobKettleParamsList["+i+"].name' value='"+kettleParamLists[i].name+"'  placeholder='请输入“参数名称”' maxlength='100' >" +
                        "</td>"+
                        " <td>" +
                        "    <input type='text' class='form-control' name='xxlJobKettleParamsList["+i+"].value' value='"+kettleParamLists[i].value+"'  placeholder='请输入“参数值”' maxlength='100' >" +
                        "       </td>" +
                        "       <td>" +
                        "      <div><button class='btn btn-danger btn-xs' _type='kettleParams_del' type='button'>删除</button> </div>" +
                        "    </td>" +
                        " </tr>");
                    $("[_type='kettleParams_del']").on("click",function() {
                        var this_name =$(this).parents("td").prev().prev().find("input").attr("name");
                        var this_value =$(this).parents("td").prev().find("input").attr("name");


                        $(this).parents("tr").nextAll().each(function(){
                            var nextName = $(this).find("[name$='.name']").attr("name") ;
                            var nextValue = $(this).find("[name$='.value']").attr("name") ;
                            $(this).find("[name$='.name']").attr("name",this_name);
                            $(this).find("[name$='.value']").attr("name",this_value);
                            this_name = nextName;
                            this_value =nextValue;
                        })
                        $(this).parents("tr").remove();

                    })
                }
            }catch(err){
                layer.open({
                    title: '系统提示',
                    content: ("原执行参数格式错误。请重新添加！"),
                    icon: '2'
                });
            }
        } else {
            $("#executeTableUpdateId").hide();
            $("#updateKettleParams").hide();
            $(".addUpdateKettleParams").hide();
        }
        if ("GLUE_PROC"==row.glueType) {
            $("#updateModal").find("#procParamLists").show();
            if(row.executorParam.indexOf("?")>-1){
                try{
                    var procParamLists = JSON.parse(row.glueSource);
                    for(var i =0;i<procParamLists.length;i++){
                        $("#updateProcParamLists").append("<tr  type='procParam'>" +
                            "             <td><label for='nub' class='control-label' color='black'>"+(i+1)+"</label></td></td> " +
                            "             <td>" +
                            "             <div >" +
                            "           <select class='form-control' name='xxlJobParamsList["+i+"].valueType'  > " +
                            "            <option value='CURSOR'" +(procParamLists[i].valueType=='CURSOR'? "selected='true'":'' )+">游标结果集</option> " +
                            "            <option value='VARCHAR'"+(procParamLists[i].valueType=='VARCHAR'? "selected='true'":'' )+" >字符串</option> " +
                            "            <option value='NUMERIC'"+(procParamLists[i].valueType=='NUMERIC'? "selected='true'":'' )+" >数字</option> " +
                            "           </select> " +
                            "          </div> " +
                            "         </td> " +
                            "         <td> " +
                            "          <div > " +
                            "           <select class='form-control' name='xxlJobParamsList["+(i)+"].paramType'  > " +
                            "            <option value='IN' "+(procParamLists[i].paramType=='IN'?"selected='true'":'' )+">输入参数</option> " +
                            "            <option value='OUT'"+(procParamLists[i].paramType=='OUT'?"selected='true'":'' )+">输出参数</option> " +
                            "           </select> " +
                            "          </div> " +
                            "         </td> " +
                            "         <td><div ><input type='text' class='form-control' name='xxlJobParamsList["+i+"].name' placeholder='请输入“参数名称”' maxlength='100' value='"+ procParamLists[i].name+"' ></div></td> " +
                            "         <td><div ><input type='text' class='form-control' name='xxlJobParamsList["+i+"].value' placeholder='请输入“参数值”' maxlength='100' value='"+procParamLists[i].value +"' ></div></td> " +
                            "         <td><div><button class='btn btn-danger btn-xs' _type='procParam_del' type='button'>删除</button> </div></td> " +
                            "        </tr>");
                        $("[_type='procParam_del']").on("click",function() {
                            var this_nub =$(this).parents("td").prev().prev().prev().prev().prev().find("label").html();
                            var this_valueType =$(this).parents("td").prev().prev().prev().prev().find("select").attr("name");
                            var this_paramsType =$(this).parents("td").prev().prev().prev().find("select").attr("name");
                            var this_name =$(this).parents("td").prev().prev().find("input").attr("name");
                            var this_value =$(this).parents("td").prev().find("input").attr("name");
                            $(this).parents("tr").nextAll().each(function(){
                                var nextNub = $(this).find("[for='nub']").html() ;
                                var nextValuesType = $(this).find("[name$='.valueType']").attr("name") ;
                                var nextParamsType = $(this).find("[name$='.paramType']").attr("name") ;
                                var nextName = $(this).find("[name$='.name']").attr("name") ;
                                var nextValue = $(this).find("[name$='.value']").attr("name") ;
                                $(this).find("[for='nub']").html(this_nub);
                                $(this).find("[name$='.valueType']").attr("name",this_valueType);
                                $(this).find("[name$='.paramType']").attr("name",this_paramsType);
                                $(this).find("[name$='.name']").attr("name",this_name);
                                $(this).find("[name$='.value']").attr("name",this_value);
                                this_nub = nextNub;
                                this_valueType = nextValuesType;
                                this_paramsType = nextParamsType;
                                this_name = nextName;
                                this_value =nextValue;
                            })
                            $(this).parents("tr").remove();

                        })
                    }
                }catch(err){
                    layer.open({
                        title: '系统提示',
                        content: ("原执行参数格式错误。请重新添加！"),
                        icon: '2'
                    });
                }
            }
        }else {
            $("#updateModal").find("#procParamLists").hide();
        }
        // show
        // base data
        $("#updateModal .form input[name='id']").val( row.id );
        $('#updateModal .form select[name=jobGroup] option[value='+ row.jobGroup +']').prop('selected', true);
        $("#updateModal .form input[name='jobDesc']").val( row.jobDesc );
        $("#updateModal .form input[name='jobCron']").val( row.jobCron );
        $("#updateModal .form input[name='author']").val( row.author );
        $("#updateModal .form input[name='alarmEmail']").val( row.alarmEmail );
        $('#updateModal .form select[name=executorRouteStrategy] option[value='+ row.executorRouteStrategy +']').prop('selected', true);
        $("#updateModal .form input[name='executorHandler']").val( row.executorHandler );
        $("#updateModal .form textarea[name='executorParam']").val( row.executorParam );
        $("#updateModal .form input[name='childJobKey']").val( row.childJobKey );
        $('#updateModal .form select[name=executorBlockStrategy] option[value='+ row.executorBlockStrategy +']').prop('selected', true);
        $('#updateModal .form select[name=executorFailStrategy] option[value='+ row.executorFailStrategy +']').prop('selected', true);
        $('#updateModal .form select[name=glueType] option[value='+ row.glueType +']').prop('selected', true);

        $("#updateModal .form select[name='glueType']").trigger("change");

        $('#updateModal').modal({backdrop: false, keyboard: false}).modal('show');
    });
    var updateModalValidate = $("#updateModal .form").validate({
        errorElement : 'span',
        errorClass : 'help-block',
        focusInvalid : true,

        rules : {
            jobDesc : {
                required : true,
                maxlength: 50
            },
            jobCron : {
                required : true
            },
            author : {
                required : true
            },
            executorParam:{
                "checkExecutorParamValue":true
            }
        },
        messages : {
            jobDesc : {
                required :"请输入“描述”."
            },
            jobCron : {
                required :"请输入“Cron”."
            },
            author : {
                required : "请输入“负责人”."
            },
            executorParam:{
                "checkExecutorParamValue": '该运行模式下“执行参数”必填'
            }
        },
        highlight : function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        success : function(label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement : function(error, element) {
            element.parent('div').append(error);
        },
        submitHandler : function(form) {
            if (checkUpdateKettle()) {
                if(checkUpdateProc()){
                    // post
                    $.post(base_url + "/jobinfo/reschedule", $("#updateModal .form").serialize(), function (data, status) {
                        if (data.code == "200") {
                            $('#updateModal').modal('hide');
                            layer.open({
                                title: '系统提示',
                                content: '更新成功',
                                icon: '1',
                                end: function (layero, index) {
                                    //window.location.reload();
                                    jobTable.fnDraw();
                                }
                            });
                        } else {
                            layer.open({
                                title: '系统提示',
                                content: (data.msg || "更新失败"),
                                icon: '2'
                            });
                        }
                    });
                }else{
                    layer.open({
                        title: '系统提示',
                        content: ("PROC模式的参数名称必填，并且执行参数“?”号数量需等于实际数量！"),
                        icon: '2'
                    });
                }
            }else{
                layer.open({
                    title: '系统提示',
                    content: ("KETTLE 模式的参数名称必填"),
                    icon: '2'
                });
            }

        }
    });

    $.validator.addMethod('checkExecutorParamValue', function(value, element, param){
        var _result = true;
        if(value=='' && ($("#addModal .glueType").val()=='GLUE_PROC'||$("#addModal .glueType").val()=='GLUE_KETTLE')){
            return false;
        }
        if(value=='' && ($("#updateModal .glueType").val()=='GLUE_PROC'||$("#updateModal .glueType").val()=='GLUE_KETTLE')){
            return false;
        }
        return _result;
    });

    var checkUpdateProc = function () {
        var flag = true;
        if($("#updateModal [name='glueType']").val()=='GLUE_PROC') {
            $("#updateProcParamLists").find("[name$='.name']").each(function () {
                if ($(this).val() == '') {
                    flag = false;
                    return;
                }
            });

            if(($("#updateModal").find("#executorParamUpdateId").val().split('?').length-1)!=$("#updateProcParamLists").find("[name$='.value']").length){
                flag = false;
            }
        }
        return flag;
    }


    var checkAddProc = function () {
        var flag = true;
        if($("#addModal [name='glueType']").val()=='GLUE_PROC') {
            $("#addProcParamLists").find("[name$='.name']").each(function () {
                if ($(this).val() == '') {
                    flag = false;
                    return;
                }
            });

            if(($("#addModal #executorParamId").val().split('?').length-1)!=$("#addProcParamLists").find("[name$='.value']").length){
                flag = false;
            }
        }
        return flag;
    }

    var checkUpdateKettle = function () {
        var flag = true;
        if($("#updateModal [name='glueType']").val()=='GLUE_KETTLE') {
            $("#updateKettleParamLists").find("[name$='.name']").each(function () {
                if ($(this).val() == '') {
                    flag = false;
                    return;
                }
            });
        }
        return flag;
    }

    var checkAddKettle = function () {
        var flag = true;
        if($("#addModal [name='glueType']").val()=='GLUE_KETTLE') {
            $("#addKettleParamLists").find("[name$='.name']").each(function () {
                if ($(this).val() == '') {
                    flag = false;
                    return;
                }
            });
        }
        return flag;
    }


    $("#updateModal").on('hide.bs.modal', function () {
        $("#updateModal .form")[0].reset()
    });

    $("#addModal").find("#executorParamId").change(function(){
        addModalBuildPorcInfo();
    });

    var addModalBuildPorcInfo = function () {
        // glueSource
        var glueType = $("#addModal").find("select[name='glueType']").val();
        var indexNo = 0;
        if(glueType=='GLUE_PROC') {
            var this_val =  $("#addModal").find("#executorParamId").val();
            var indexNub = (this_val.split('?')).length - 1;
            var procParms = $("#addModal").find("tr[type='procParam']");
            if (indexNub > procParms.length) {
                var needAddNub = indexNub - procParms.length;
                for (var i = 0; i < needAddNub; i++) {
                    $("#addProcParamLists").append("<tr type='procParam'>" +
                        "             <td><label for='nub' class='control-label' color='black'>"+(procParms.length + i + 1)+"</label></td></td> " +
                        "             <td>" +
                        "             <div >" +
                        "           <select class='form-control' name='xxlJobParamsList["+(procParms.length + i )+"].valueType'  > " +
                        "            <option value='CURSOR'>游标结果集</option> " +
                        "            <option value='VARCHAR' >字符串</option> " +
                        "            <option value='NUMERIC'>数字</option> " +
                        "           </select> " +
                        "          </div> " +
                        "         </td> " +
                        "         <td> " +
                        "          <div > " +
                        "           <select class='form-control' name='xxlJobParamsList["+(procParms.length + i )+"].paramType'  > " +
                        "            <option value='IN' >输入参数</option> " +
                        "            <option value='OUT'>输出参数</option> " +
                        "           </select> " +
                        "          </div> " +
                        "         </td> " +
                        "         <td><div ><input type='text' class='form-control' name='xxlJobParamsList["+(procParms.length + i )+"].name' placeholder='请输入“参数名称”' maxlength='100'  ></div></td> " +
                        "         <td><div ><input type='text' class='form-control' name='xxlJobParamsList["+(procParms.length + i )+"].value' placeholder='请输入“参数值”' maxlength='100' ></div></td> " +
                        "         <td><div><button class='btn btn-danger btn-xs' _type='procParam_del' type='button'>删除</button> </div></td> " +
                        "        </tr>");

                    $("[_type='procParam_del']").on("click",function() {
                        var this_nub =$(this).parents("td").prev().prev().prev().prev().prev().find("label").html();
                        var this_valueType =$(this).parents("td").prev().prev().prev().prev().find("select").attr("name");
                        var this_paramsType =$(this).parents("td").prev().prev().prev().find("select").attr("name");
                        var this_name =$(this).parents("td").prev().prev().find("input").attr("name");
                        var this_value =$(this).parents("td").prev().find("input").attr("name");
                        $(this).parents("tr").nextAll().each(function(){
                            var nextNub = $(this).find("[for='nub']").html() ;
                            var nextValuesType = $(this).find("[name$='.valueType']").attr("name") ;
                            var nextParamsType = $(this).find("[name$='.paramType']").attr("name") ;
                            var nextName = $(this).find("[name$='.name']").attr("name") ;
                            var nextValue = $(this).find("[name$='.value']").attr("name") ;
                            $(this).find("[for='nub']").html(this_nub);
                            $(this).find("[name$='.valueType']").attr("name",this_valueType);
                            $(this).find("[name$='.paramType']").attr("name",this_paramsType);
                            $(this).find("[name$='.name']").attr("name",this_name);
                            $(this).find("[name$='.value']").attr("name",this_value);
                            this_nub = nextNub;
                            this_valueType = nextValuesType;
                            this_paramsType = nextParamsType;
                            this_name = nextName;
                            this_value =nextValue;
                        })
                        $(this).parents("tr").remove();

                    })
                }
            }
        }
    }


    $("#updateModal").find("#executorParamUpdateId").change(function(){
        // glueSource
        var glueType = $("#updateModal").find("select[name='glueType']").val();
        var indexNo = 0;
        if(glueType=='GLUE_PROC') {
            var this_val = $(this).val();
            var indexNub = (this_val.split('?')).length - 1;
            var procParms = $("#updateModal").find("tr[type='procParam']");
            if (indexNub > procParms.length) {
                var needAddNub = indexNub - procParms.length;
                for (var i = 0; i < needAddNub; i++) {
                    if (procParms.length > 0) {
                        $("#updateProcParamLists").append("<tr  type='procParam'>" +
                            "             <td><label for='nub' class='control-label'>"+(procParms.length + i + 1)+"</label></td></td> " +
                            "             <td>" +
                            "             <div >" +
                            "           <select class='form-control' name='xxlJobParamsList["+(procParms.length + i)+"].valueType'  > " +
                            "            <option value='CURSOR'>游标结果集</option> " +
                            "            <option value='VARCHAR' >字符串</option> " +
                            "            <option value='NUMERIC'>数字</option> " +
                            "           </select> " +
                            "          </div> " +
                            "         </td> " +
                            "         <td> " +
                            "          <div > " +
                            "           <select class='form-control' name='xxlJobParamsList["+(procParms.length + i )+"].paramType'  > " +
                            "            <option value='IN' >输入参数</option> " +
                            "            <option value='OUT' >输出参数</option> " +
                            "           </select> " +
                            "          </div> " +
                            "         </td> " +
                            "         <td><div ><input type='text' class='form-control' name='xxlJobParamsList["+(procParms.length + i)+"].name' placeholder='请输入“参数名称”' maxlength='100' ></div></td> " +
                            "         <td><div ><input type='text' class='form-control' name='xxlJobParamsList["+(procParms.length + i)+"].value' placeholder='请输入“参数值”' maxlength='100' ></div></td> " +
                            "         <td><div><button class='btn btn-danger btn-xs' _type='procParam_del' type='button'>删除</button> </div></td> " +
                            "        </tr>");
                    } else {
                        $("#updateProcParamLists").append("<tr  type='procParam'>" +
                            "             <td><label for='nub' class='control-label'>"+(needAddNub-1)+"</label></td>" +
                            "             <td>" +
                            "             <div >" +
                            "           <select class='form-control' name='xxlJobParamsList["+(needAddNub-1)+"].valueType'  > " +
                            "            <option value='CURSOR'>游标结果集</option> " +
                            "            <option value='VARCHAR' >字符串</option> " +
                            "            <option value='NUMERIC'>数字</option> " +
                            "           </select> " +
                            "          </div> " +
                            "         </td> " +
                            "         <td> " +
                            "          <div > " +
                            "           <select class='form-control' name='xxlJobParamsList["+(needAddNub-1)+"].paramType'  > " +
                            "            <option value='IN' >输入参数</option> " +
                            "            <option value='OUT' >输出参数</option> " +
                            "           </select> " +
                            "          </div> " +
                            "         </td> " +
                            "         <td><div ><input type='text' class='form-control' name='xxlJobParamsList["+(needAddNub-1)+"].name' placeholder='请输入“参数名称”' maxlength='100' ></div></td> " +
                            "         <td><div ><input type='text' class='form-control' name='xxlJobParamsList["+(needAddNub-1)+"].value' placeholder='请输入“参数值”' maxlength='100' ></div></td> " +
                            "         <td><div><button class='btn btn-danger btn-xs' _type='procParam_del' type='button'>删除</button> </div></td> " +
                            "        </tr>");
                    }
                    $("[_type='procParam_del']").on("click",function() {
                        var this_nub =$(this).parents("td").prev().prev().prev().prev().prev().find("label").html();
                        var this_valueType =$(this).parents("td").prev().prev().prev().prev().find("select").attr("name");
                        var this_paramsType =$(this).parents("td").prev().prev().prev().find("select").attr("name");
                        var this_name =$(this).parents("td").prev().prev().find("input").attr("name");
                        var this_value =$(this).parents("td").prev().find("input").attr("name");
                        $(this).parents("tr").nextAll().each(function(){
                            var nextNub = $(this).find("[for='nub']").html() ;
                            var nextValuesType = $(this).find("[name$='.valueType']").attr("name") ;
                            var nextParamsType = $(this).find("[name$='.paramType']").attr("name") ;
                            var nextName = $(this).find("[name$='.name']").attr("name") ;
                            var nextValue = $(this).find("[name$='.value']").attr("name") ;
                            $(this).find("[for='nub']").html(this_nub);
                            $(this).find("[name$='.valueType']").attr("name",this_valueType);
                            $(this).find("[name$='.paramType']").attr("name",this_paramsType);
                            $(this).find("[name$='.name']").attr("name",this_name);
                            $(this).find("[name$='.value']").attr("name",this_value);
                            this_nub = nextNub;
                            this_valueType = nextValuesType;
                            this_paramsType = nextParamsType;
                            this_name = nextName;
                            this_value =nextValue;
                        })
                        $(this).parents("tr").remove();

                    })
                }
            }
        }

    });

    $(".addKettleParams").click(function(){
        $("#addKettleParams").show();
        var kettleParamListsLength = $("[type='updateKettleParamLists']").length;
        $("#addKettleParamLists").append("<tr type='kettleParamLists'>" +
            "                                     <td>" +
            "                                         <input type='text' class='form-control' name='xxlJobKettleParamsList["+(kettleParamListsLength)+"].name' value=''  placeholder='请输入“参数名称”' maxlength='100' >" +
            "                                     </td>" +
            "                                     <td>" +
            "                                         <input type='text' class='form-control' name='xxlJobKettleParamsList["+(kettleParamListsLength)+"].value' value=''  placeholder='请输入“参数值”' maxlength='100' >" +
            "                                     </td>" +
            "                                     <td>" +
            "                                         <div><button class='btn btn-danger btn-xs' _type='kettleParams_del' type='button'>删除</button> </div>" +
            "                                     </td>" +
            "                                 </tr>");
        $("[_type='kettleParams_del']").on("click",function() {
            var this_name =$(this).parents("td").prev().prev().find("input").attr("name");
            var this_value =$(this).parents("td").prev().find("input").attr("name");
            $(this).parents("tr").nextAll().each(function(){
                var nextName = $(this).find("[name$='.name']").attr("name") ;
                var nextValue = $(this).find("[name$='.value']").attr("name") ;
                $(this).find("[name$='.name']").attr("name",this_name);
                $(this).find("[name$='.value']").attr("name",this_value);
                this_name = nextName;
                this_value =nextValue;
            })
            $(this).parents("tr").remove();

        })
        $("[_type='kettleParams_del']").on("click",function() {
            var this_name =$(this).parents("td").prev().prev().find("input").attr("name");
            var this_value =$(this).parents("td").prev().find("input").attr("name");
            $(this).parents("tr").nextAll().each(function(){
                var nextName = $(this).find("[name$='.name']").attr("name") ;
                var nextValue = $(this).find("[name$='.value']").attr("name") ;
                $(this).find("[name$='.name']").attr("name",this_name);
                $(this).find("[name$='.value']").attr("name",this_value);
                this_name = nextName;
                this_value =nextValue;
            })
            $(this).parents("tr").remove();

        })
    });

    $(".addUpdateKettleParams").click(function(){
        $("#updateKettleParams").show();
        var kettleParamListsLength = $("[type='updateKettleParamLists']").length;
        $("#updateKettleParamLists").append("<tr type='updateKettleParamLists'>" +
            "                                     <td>" +
            "                                         <input type='text' class='form-control' name='xxlJobKettleParamsList["+(kettleParamListsLength)+"].name' value=''  placeholder='请输入“参数名称”' maxlength='100' >" +
            "                                     </td>" +
            "                                     <td>" +
            "                                         <input type='text' class='form-control' name='xxlJobKettleParamsList["+(kettleParamListsLength)+"].value' value=''  placeholder='请输入“参数值”' maxlength='100' >" +
            "                                     </td>" +
            "                                     <td>" +
            "                                         <div><button class='btn btn-danger btn-xs' _type='kettleParams_del' type='button'>删除</button> </div>" +
            "                                     </td>" +
            "                                 </tr>");
        $("[_type='kettleParams_del']").on("click",function() {
            var this_name =$(this).parents("td").prev().prev().find("input").attr("name");
            var this_value =$(this).parents("td").prev().find("input").attr("name");
            $(this).parents("tr").nextAll().each(function(){
                var nextName = $(this).find("[name$='.name']").attr("name") ;
                var nextValue = $(this).find("[name$='.value']").attr("name") ;
                $(this).find("[name$='.name']").attr("name",this_name);
                $(this).find("[name$='.value']").attr("name",this_value);
                this_name = nextName;
                this_value =nextValue;
            })
            $(this).parents("tr").remove();
        })
    });


});
var initExecutorNameSelect2 = function(prodId,valId,url,placeholder,params){
    var $prod = $("#" + prodId);
    $prod.select2({
        ajax: {
            type:'GET',
            url: url,
            dataType: 'json',
            data: function (term, page) {
                var p = {vcName:term, page:page,pageSize:50};
                if(params!="undefined" && params!=null){
                    p = $.extend(p, params);
                }
                return p;
            },
            results: function (data, params) {
                var d = {results: []};
                $.each(data, function(i, item) {
                    d.results.push({id: item.id, text: item.tabName,jobId:item.jobId});
                });
                return d;
            },
            cache: false
        },
        placeholder:placeholder,
        language: "zh-CN",
        allowClear: true,
        height:"38px",
        escapeMarkup: function (markup) { return markup; },
        formatResult: function formatRepo(repo){return repo.text;}, // 函数用来渲染结果
        formatSelection: function formatRepoSelection(repo){
            $("#"+valId).val(repo.id);
            return repo.text;
        } // 函数用于呈现当前的选择
    });
};
