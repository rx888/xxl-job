package com.xxl.job.admin.core.model;

/**
 * @author: create by zhaozg
 * @version: v1.0
 * @description: com.xxl.job.admin.core.model
 * @date:2019/2/20
 */
public class XxlJobParams {

    private int index;
    private String valueType;
    private String value;
    private String paramType;
    private String name;


    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
