package com.xxl.job.admin.core.model;

/**
 * @author: create by zhaozg
 * @version: v1.0
 * @description: com.xxl.job.admin.core.model
 * @date:2019/2/20
 */
public class XxlJobKettleParams {

    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
