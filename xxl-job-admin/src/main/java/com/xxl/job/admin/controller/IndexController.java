package com.xxl.job.admin.controller;

import com.kaiyufound.kres.utils.security.AESUtil;
import com.kaiyufound.kres.utils.security.RSAUtil;
import com.kaiyufound.kres.utils.spring.SpringContextUtil;
import com.kaiyufound.kres.utils.time.DateUtils;
import com.xxl.job.admin.controller.annotation.PermessionLimit;
import com.xxl.job.admin.controller.interceptor.PermissionInterceptor;
import com.xxl.job.admin.service.XxlJobService;
import com.xxl.job.core.biz.model.ReturnT;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.PrivateKey;
import java.util.Map;

/**
 * index controller
 * @author xuxueli 2015-12-19 16:13:16
 */
@Controller
public class IndexController {

	/**
	 * LOGGER 日志 .
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

	@Resource
	private XxlJobService xxlJobService;

	@RequestMapping("/")
	public String index(Model model) {

		Map<String, Object> dashboardMap = xxlJobService.dashboardInfo();
		model.addAllAttributes(dashboardMap);

		return "index";
	}

    @RequestMapping("/triggerChartDate")
	@ResponseBody
	public ReturnT<Map<String, Object>> triggerChartDate() {
        ReturnT<Map<String, Object>> triggerChartDate = xxlJobService.triggerChartDate();
        return triggerChartDate;
    }
	
	@RequestMapping("/toLogin")
	@PermessionLimit(limit=false)
	public String toLogin(Model model, HttpServletRequest request) {
		if (PermissionInterceptor.ifLogin(request)) {
			return "redirect:/";
		}
		return "login";
	}
	
	@RequestMapping(value="login", method=RequestMethod.POST)
	@ResponseBody
	@PermessionLimit(limit=false)
	public ReturnT<String> loginDo(HttpServletRequest request, HttpServletResponse response, String userName, String password, String ifRemember){
		if (!PermissionInterceptor.ifLogin(request)) {
			// PropertiesUtil改用SpringContextUtil
			String securityPassword = SpringContextUtil.getProperty("xxl.job.login.password");
			String encryptKey = new String(Base64.decodeBase64(SpringContextUtil.getProperty("system.security.key")));
			if (StringUtils.isNotBlank(userName) && StringUtils.isNotBlank(password)
					&& SpringContextUtil.getProperty("xxl.job.login.username").equals(userName)
					&& securityPassword.equals(AESUtil.encrypt(password, encryptKey))) {
				boolean ifRem = false;
				if (StringUtils.isNotBlank(ifRemember) && "on".equals(ifRemember)) {
					ifRem = true;
				}
				PermissionInterceptor.login(response, ifRem);
			} else {
				return new ReturnT<String>(500, "账号或密码错误");
			}
		}
		return ReturnT.SUCCESS;
	}
	
	@RequestMapping(value="logout", method=RequestMethod.POST)
	@ResponseBody
	@PermessionLimit(limit=false)
	public ReturnT<String> logout(HttpServletRequest request, HttpServletResponse response){
		if (PermissionInterceptor.ifLogin(request)) {
			PermissionInterceptor.logout(request, response);
		}
		return ReturnT.SUCCESS;
	}
	
	@RequestMapping("/help")
	public String help() {

		/*if (!PermissionInterceptor.ifLogin(request)) {
			return "redirect:/toLogin";
		}*/

		return "help";
	}

	@RequestMapping("/ssoLogin")
	@PermessionLimit(limit = false)
	public String ssoLogin(HttpServletRequest request, HttpServletResponse response) {
		//获取接口参数
		String tokenName = SpringContextUtil.getProperty("xxl.job.login.tokenname", "token");
		String httpToken = request.getParameter(tokenName);
		if (StringUtils.isEmpty(httpToken)) {
			//跳回主页,此时为未登录状态,禁止访问
			PermissionInterceptor.logout(request, response);
			return "redirect:/";
		}
		//获取配置的用户名
		String userName = SpringContextUtil.getProperty("xxl.job.login.username", "admin");
		String dateFormat = DateUtils.format(DateUtils.now(), SpringContextUtil.getProperty("xxl.job.login.dateformat", "yyyyMMdd"));
		//Token经过加密处理,规则: userName + DateFormat(日期yyyyMMdd)
		String token = userName + dateFormat;
		String tokenTemp = "";
		try {
			//私钥解密
			String privateKeyStr = SpringContextUtil.getProperty("xxl.job.admin.privatekey");
			PrivateKey privateKey = RSAUtil.generatePrivateKey(privateKeyStr);
			//对request中传递的参数"token"进行解密
			tokenTemp = new String(RSAUtil.decrypt(Base64.decodeBase64(httpToken), privateKey), "UTF-8");
		} catch (Exception e) {
			LOGGER.error("xxl-job admin sso login error:{}, because:{}", token, e.getStackTrace());
		}
		//token解密一致则允许登录
		if (token.equals(tokenTemp)) {
			PermissionInterceptor.login(response, false);
		}
		return "redirect:/";
	}
	
}
