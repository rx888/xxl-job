/**
 * XxlJobExecuteTable .
 * <p>
 * Desc： .
 * Name： XxlJobExecuteTable .
 * <p>
 * version    date         company      users       content
 * ———————————————————————————————————————————————————————------
 * Ver    2018/11/21    Kaiyufound    chenhao
 * <p>
 * Copyright &copy; 2018 <a href="http://www.kaiyufound.com/">Kaiyu</a> All rights reserved .
 */
package com.xxl.job.admin.core.model;

/**
 * @author chenhao .
 * @className XxlJobExecuteTable .
 * @description TODO .
 * @dateTime 2018/11/21 下午 6:47 .
 * @see         .
 * @version Ver <版本>-<状态> .
 * @since       <DSS_DEV>JDK 1.6 .
 */
public class XxlJobExecuteTable {
    private String tabName;//目标表;本地表;
    private String tabSrcs;//来源标识;
    private String tabCmm;//对象表注释;
    private int tabRows;//目标表数据数;
    private int vali;//是否有效标识;
    private int valiCnt;//是否统计标识;
    private String srcTabs;//数据来源表;
    private int srcRows;//来源表数据量;
    private String tmpTabs;//临时数据表;
    private String tabKeys;//逻辑主键;
    private String etlJob;//任务文件名;Kettle文件;
    private String etlJobAuto;//自动任务文件名;Kettle文件;
    private String jobId;//jobid
    private String id;

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

    public String getTabSrcs() {
        return tabSrcs;
    }

    public void setTabSrcs(String tabSrcs) {
        this.tabSrcs = tabSrcs;
    }

    public String getTabCmm() {
        return tabCmm;
    }

    public void setTabCmm(String tabCmm) {
        this.tabCmm = tabCmm;
    }

    public int getTabRows() {
        return tabRows;
    }

    public void setTabRows(int tabRows) {
        this.tabRows = tabRows;
    }

    public int getVali() {
        return vali;
    }

    public void setVali(int vali) {
        this.vali = vali;
    }

    public int getValiCnt() {
        return valiCnt;
    }

    public void setValiCnt(int valiCnt) {
        this.valiCnt = valiCnt;
    }

    public String getSrcTabs() {
        return srcTabs;
    }

    public void setSrcTabs(String srcTabs) {
        this.srcTabs = srcTabs;
    }

    public int getSrcRows() {
        return srcRows;
    }

    public void setSrcRows(int srcRows) {
        this.srcRows = srcRows;
    }

    public String getTmpTabs() {
        return tmpTabs;
    }

    public void setTmpTabs(String tmpTabs) {
        this.tmpTabs = tmpTabs;
    }

    public String getTabKeys() {
        return tabKeys;
    }

    public void setTabKeys(String tabKeys) {
        this.tabKeys = tabKeys;
    }

    public String getEtlJob() {
        return etlJob;
    }

    public void setEtlJob(String etlJob) {
        this.etlJob = etlJob;
    }

    public String getEtlJobAuto() {
        return etlJobAuto;
    }

    public void setEtlJobAuto(String etlJobAuto) {
        this.etlJobAuto = etlJobAuto;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}