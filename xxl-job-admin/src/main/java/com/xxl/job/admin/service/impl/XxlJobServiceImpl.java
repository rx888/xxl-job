package com.xxl.job.admin.service.impl;

import com.google.common.collect.Lists;
import com.xxl.job.admin.core.enums.ExecutorFailStrategyEnum;
import com.xxl.job.admin.core.model.*;
import com.xxl.job.admin.core.route.ExecutorRouteStrategyEnum;
import com.xxl.job.admin.core.schedule.XxlJobDynamicScheduler;
import com.xxl.job.admin.dao.XxlJobGroupDao;
import com.xxl.job.admin.dao.XxlJobInfoDao;
import com.xxl.job.admin.dao.XxlJobLogDao;
import com.xxl.job.admin.dao.XxlJobLogGlueDao;
import com.xxl.job.admin.service.XxlJobService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.enums.ExecutorBlockStrategyEnum;
import com.xxl.job.core.glue.GlueTypeEnum;
import com.xxl.job.core.util.JacksonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.quartz.CronExpression;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;

/**
 * core job action for xxl-job
 * @author xuxueli 2016-5-28 15:30:33
 */
@Service
public class XxlJobServiceImpl implements XxlJobService {
	private static Logger logger = LoggerFactory.getLogger(XxlJobServiceImpl.class);

	@Resource
	private XxlJobGroupDao xxlJobGroupDao;
	@Resource
	private XxlJobInfoDao xxlJobInfoDao;
	@Resource
	public XxlJobLogDao xxlJobLogDao;
	@Resource
	private XxlJobLogGlueDao xxlJobLogGlueDao;

	@Override
	public Map<String, Object> pageList(int start, int length, int jobGroup, String executorHandler, String filterTime,String executorName) {

		// page list
		List<XxlJobInfo> list = xxlJobInfoDao.pageList(start, length, jobGroup, executorHandler,executorName);
		int list_count = xxlJobInfoDao.pageListCount(start, length, jobGroup, executorHandler,executorName);

		// fill job info
		if (list!=null && list.size()>0) {
			for (XxlJobInfo jobInfo : list) {
				XxlJobDynamicScheduler.fillJobInfo(jobInfo);
			}
		}

		// package result
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("recordsTotal", list_count);		// 总记录数
		maps.put("recordsFiltered", list_count);	// 过滤后的总记录数
		maps.put("data", list);  					// 分页列表
		return maps;
	}

	@Override
	public ReturnT<String> add(XxlJobInfo jobInfo) {
		// valid
		XxlJobGroup group = xxlJobGroupDao.load(jobInfo.getJobGroup());
		if (group == null) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "请选择“执行器”");
		}
		if (!CronExpression.isValidExpression(jobInfo.getJobCron())) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "请输入格式正确的“Cron”");
		}
		if (StringUtils.isBlank(jobInfo.getJobDesc())) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "请输入“任务描述”");
		}
		if (StringUtils.isBlank(jobInfo.getAuthor())) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "请输入“负责人”");
		}
		if (ExecutorRouteStrategyEnum.match(jobInfo.getExecutorRouteStrategy(), null) == null) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "路由策略非法");
		}
		if (ExecutorBlockStrategyEnum.match(jobInfo.getExecutorBlockStrategy(), null) == null) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "阻塞处理策略非法");
		}
		if (ExecutorFailStrategyEnum.match(jobInfo.getExecutorFailStrategy(), null) == null) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "失败处理策略非法");
		}
		if (GlueTypeEnum.match(jobInfo.getGlueType()) == null) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "运行模式非法非法");
		}
		if (GlueTypeEnum.BEAN==GlueTypeEnum.match(jobInfo.getGlueType()) && StringUtils.isBlank(jobInfo.getExecutorHandler())) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "请输入“JobHandler”");
		}

		// fix "\r" in shell
		if (GlueTypeEnum.GLUE_SHELL==GlueTypeEnum.match(jobInfo.getGlueType()) && jobInfo.getGlueSource()!=null) {
			jobInfo.setGlueSource(jobInfo.getGlueSource().replaceAll("\r", ""));
		}

		// childJobKey valid
		if (StringUtils.isNotBlank(jobInfo.getChildJobKey())) {
			String[] childJobKeys = jobInfo.getChildJobKey().split(",");
			for (String childJobKeyItem: childJobKeys) {
				String[] childJobKeyArr = childJobKeyItem.split("_");
				if (childJobKeyArr.length!=2) {
					return new ReturnT<String>(ReturnT.FAIL_CODE, MessageFormat.format("子任务Key({0})格式错误", childJobKeyItem));
				}
				XxlJobInfo childJobInfo = xxlJobInfoDao.loadById(Integer.valueOf(childJobKeyArr[1]));
				if (childJobInfo==null) {
					return new ReturnT<String>(ReturnT.FAIL_CODE, MessageFormat.format("子任务Key({0})无效", childJobKeyItem));
				}
			}
		}
		if("GLUE_PROC".equals(jobInfo.getGlueType())){
			if(jobInfo.getXxlJobParamsList()!=null) {
				for (int i = 0; i < jobInfo.getXxlJobParamsList().size(); i++) {
					jobInfo.getXxlJobParamsList().get(i).setIndex(i + 1);
				}
			}else{
				List <XxlJobParams> xxlJobParamsList = Lists.newArrayList();
				jobInfo.setXxlJobParamsList(xxlJobParamsList);
			}
			String glueSource = JacksonUtil.writeValueAsString(jobInfo.getXxlJobParamsList()).replace("},{", "},\n{");
			jobInfo.setGlueSource(glueSource);
			jobInfo.setGlueRemark(com.kaiyufound.kres.utils.time.DateUtils.format(new Date(), "yyyyMMdd") + jobInfo.getJobDesc());
			jobInfo.setGlueUpdatetime(new Date());
			xxlJobInfoDao.save(jobInfo);

			//添加历史版本
			XxlJobLogGlue xxlJobLogGlue = new XxlJobLogGlue();
			xxlJobLogGlue.setJobId(jobInfo.getId());
			xxlJobLogGlue.setGlueType(jobInfo.getGlueType());
			xxlJobLogGlue.setGlueSource(glueSource);
			xxlJobLogGlue.setGlueRemark(jobInfo.getGlueRemark());
			xxlJobLogGlueDao.save(xxlJobLogGlue);
		}else if("GLUE_KETTLE".equals(jobInfo.getGlueType())){
			if(jobInfo.getXxlJobKettleParamsList()==null) {
				List <XxlJobKettleParams> xxlJobKettleParamsList = Lists.newArrayList();
				jobInfo.setXxlJobKettleParamsList(xxlJobKettleParamsList);
			}
			String glueSource  = JacksonUtil.writeValueAsString(jobInfo.getXxlJobKettleParamsList()).replace("},{","},\n{");;
			jobInfo.setGlueSource(glueSource);
			jobInfo.setGlueRemark(com.kaiyufound.kres.utils.time.DateUtils.format(new Date(),"yyyyMMdd") + jobInfo.getJobDesc());
			jobInfo.setGlueUpdatetime(new Date());
			xxlJobInfoDao.save(jobInfo);

			//添加历史版本
			XxlJobLogGlue xxlJobLogGlue = new XxlJobLogGlue();
			xxlJobLogGlue.setJobId(jobInfo.getId());
			xxlJobLogGlue.setGlueType(jobInfo.getGlueType());
			xxlJobLogGlue.setGlueSource(glueSource);
			xxlJobLogGlue.setGlueRemark(jobInfo.getGlueRemark());
			xxlJobLogGlueDao.save(xxlJobLogGlue);
		}else{
			xxlJobInfoDao.save(jobInfo);
		}
		// add in db
		if (jobInfo.getId() < 1) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "新增任务失败");
		}

		// add in quartz
		String qz_group = String.valueOf(jobInfo.getJobGroup());
		String qz_name = String.valueOf(jobInfo.getId());
		try {
			XxlJobDynamicScheduler.addJob(qz_name, qz_group, jobInfo.getJobCron());
			//XxlJobDynamicScheduler.pauseJob(qz_name, qz_group);
			return ReturnT.SUCCESS;
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
			try {
				xxlJobInfoDao.delete(jobInfo.getId());
				XxlJobDynamicScheduler.removeJob(qz_name, qz_group);
			} catch (SchedulerException e1) {
				logger.error(e.getMessage(), e1);
			}
			return new ReturnT<String>(ReturnT.FAIL_CODE, "新增任务失败:" + e.getMessage());
		}
	}

	@Override
	public ReturnT<String> reschedule(XxlJobInfo jobInfo) {

		// valid
		if (!CronExpression.isValidExpression(jobInfo.getJobCron())) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "请输入格式正确的“Cron”");
		}
		if (StringUtils.isBlank(jobInfo.getJobDesc())) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "请输入“任务描述”");
		}
		if (StringUtils.isBlank(jobInfo.getAuthor())) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "请输入“负责人”");
		}
		if (ExecutorRouteStrategyEnum.match(jobInfo.getExecutorRouteStrategy(), null) == null) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "路由策略非法");
		}
		if (ExecutorBlockStrategyEnum.match(jobInfo.getExecutorBlockStrategy(), null) == null) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "阻塞处理策略非法");
		}
		if (ExecutorFailStrategyEnum.match(jobInfo.getExecutorFailStrategy(), null) == null) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "失败处理策略非法");
		}

		// childJobKey valid
		if (StringUtils.isNotBlank(jobInfo.getChildJobKey())) {
			String[] childJobKeys = jobInfo.getChildJobKey().split(",");
			for (String childJobKeyItem: childJobKeys) {
				String[] childJobKeyArr = childJobKeyItem.split("_");
				if (childJobKeyArr.length!=2) {
					return new ReturnT<String>(ReturnT.FAIL_CODE, MessageFormat.format("子任务Key({0})格式错误", childJobKeyItem));
				}
				XxlJobInfo childJobInfo = xxlJobInfoDao.loadById(Integer.valueOf(childJobKeyArr[1]));
				if (childJobInfo==null) {
					return new ReturnT<String>(ReturnT.FAIL_CODE, MessageFormat.format("子任务Key({0})无效", childJobKeyItem));
				}
			}
		}

		// stage job info
		XxlJobInfo exists_jobInfo = xxlJobInfoDao.loadById(jobInfo.getId());
		if (exists_jobInfo == null) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "参数异常");
		}
		//String old_cron = exists_jobInfo.getJobCron();

		exists_jobInfo.setJobCron(jobInfo.getJobCron());
		exists_jobInfo.setJobDesc(jobInfo.getJobDesc());
		exists_jobInfo.setAuthor(jobInfo.getAuthor());
		exists_jobInfo.setAlarmEmail(jobInfo.getAlarmEmail());
		exists_jobInfo.setExecutorRouteStrategy(jobInfo.getExecutorRouteStrategy());
		exists_jobInfo.setExecutorHandler(jobInfo.getExecutorHandler());
		exists_jobInfo.setExecutorParam(jobInfo.getExecutorParam());
		exists_jobInfo.setExecutorBlockStrategy(jobInfo.getExecutorBlockStrategy());
		exists_jobInfo.setExecutorFailStrategy(jobInfo.getExecutorFailStrategy());
		exists_jobInfo.setChildJobKey(jobInfo.getChildJobKey());
		String glueSource ="";
		if("GLUE_PROC".equals(exists_jobInfo.getGlueType())){
			if(jobInfo.getXxlJobParamsList()!=null) {
				for (int i = 0; i < jobInfo.getXxlJobParamsList().size(); i++) {
					jobInfo.getXxlJobParamsList().get(i).setIndex(i + 1);
				}
			}else{
				List <XxlJobParams> xxlJobParamsList = Lists.newArrayList();
				jobInfo.setXxlJobParamsList(xxlJobParamsList);
			}
			glueSource  = JacksonUtil.writeValueAsString(jobInfo.getXxlJobParamsList()).replace("},{","},\n{");
			exists_jobInfo.setGlueSource(glueSource);
			exists_jobInfo.setGlueRemark(com.kaiyufound.kres.utils.time.DateUtils.format(new Date(),"yyyyMMdd") + exists_jobInfo.getJobDesc());
			exists_jobInfo.setGlueUpdatetime(new Date());

			//添加历史版本
			XxlJobLogGlue xxlJobLogGlue = new XxlJobLogGlue();
			xxlJobLogGlue.setJobId(exists_jobInfo.getId());
			xxlJobLogGlue.setGlueType(exists_jobInfo.getGlueType());
			xxlJobLogGlue.setGlueSource(glueSource);
			xxlJobLogGlue.setGlueRemark(exists_jobInfo.getGlueRemark());
			xxlJobLogGlueDao.save(xxlJobLogGlue);

			// remove code backup more than 30
			xxlJobLogGlueDao.removeOld(exists_jobInfo.getId(), 30);
		}
		if("GLUE_KETTLE".equals(exists_jobInfo.getGlueType())){
			if(jobInfo.getXxlJobKettleParamsList()==null) {
				List <XxlJobKettleParams> xxlJobKettleParamsList = Lists.newArrayList();
				jobInfo.setXxlJobKettleParamsList(xxlJobKettleParamsList);
			}
			glueSource  = JacksonUtil.writeValueAsString(jobInfo.getXxlJobKettleParamsList()).replace("},{","},\n{");;
			exists_jobInfo.setGlueSource(glueSource);
			exists_jobInfo.setGlueRemark(com.kaiyufound.kres.utils.time.DateUtils.format(new Date(),"yyyyMMdd") + exists_jobInfo.getJobDesc());
			exists_jobInfo.setGlueUpdatetime(new Date());

			//添加历史版本
			XxlJobLogGlue xxlJobLogGlue = new XxlJobLogGlue();
			xxlJobLogGlue.setJobId(exists_jobInfo.getId());
			xxlJobLogGlue.setGlueType(exists_jobInfo.getGlueType());
			xxlJobLogGlue.setGlueSource(glueSource);
			xxlJobLogGlue.setGlueRemark(exists_jobInfo.getGlueRemark());
			xxlJobLogGlueDao.save(xxlJobLogGlue);

			// remove code backup more than 30
			xxlJobLogGlueDao.removeOld(exists_jobInfo.getId(), 30);
		}
		xxlJobInfoDao.update(exists_jobInfo);

		// fresh quartz
		String qz_group = String.valueOf(exists_jobInfo.getJobGroup());
		String qz_name = String.valueOf(exists_jobInfo.getId());
		try {
			boolean ret = XxlJobDynamicScheduler.rescheduleJob(qz_group, qz_name, exists_jobInfo.getJobCron());
			return ret?ReturnT.SUCCESS:ReturnT.FAIL;
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
		}

		return ReturnT.FAIL;
	}

	@Override
	public ReturnT<String> remove(int id) {
		XxlJobInfo xxlJobInfo = xxlJobInfoDao.loadById(id);
		String group = String.valueOf(xxlJobInfo.getJobGroup());
		String name = String.valueOf(xxlJobInfo.getId());

		try {
			XxlJobDynamicScheduler.removeJob(name, group);
			xxlJobInfoDao.delete(id);
			xxlJobLogDao.delete(id);
			xxlJobLogGlueDao.deleteByJobId(id);
			return ReturnT.SUCCESS;
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
		}
		return ReturnT.FAIL;
	}

	@Override
	public ReturnT<String> pause(int id) {
		XxlJobInfo xxlJobInfo = xxlJobInfoDao.loadById(id);
		String group = String.valueOf(xxlJobInfo.getJobGroup());
		String name = String.valueOf(xxlJobInfo.getId());

		try {
			boolean ret = XxlJobDynamicScheduler.pauseJob(name, group);	// jobStatus do not store
			return ret?ReturnT.SUCCESS:ReturnT.FAIL;
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
			return ReturnT.FAIL;
		}
	}

	@Override
	public ReturnT<String> resume(int id) {
		XxlJobInfo xxlJobInfo = xxlJobInfoDao.loadById(id);
		String group = String.valueOf(xxlJobInfo.getJobGroup());
		String name = String.valueOf(xxlJobInfo.getId());

		try {
			boolean ret = XxlJobDynamicScheduler.resumeJob(name, group);
			return ret?ReturnT.SUCCESS:ReturnT.FAIL;
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
			return ReturnT.FAIL;
		}
	}

	@Override
	public ReturnT<String> triggerJob(int id) {
		XxlJobInfo xxlJobInfo = xxlJobInfoDao.loadById(id);
		if (xxlJobInfo == null) {
			return new ReturnT<String>(ReturnT.FAIL_CODE, "任务ID非法");
		}

		String group = String.valueOf(xxlJobInfo.getJobGroup());
		String name = String.valueOf(xxlJobInfo.getId());

		try {
			XxlJobDynamicScheduler.triggerJob(name, group);
			return ReturnT.SUCCESS;
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
			return new ReturnT<String>(ReturnT.FAIL_CODE, e.getMessage());
		}
	}

	@Override
	public Map<String, Object> dashboardInfo() {

		int jobInfoCount = xxlJobInfoDao.findAllCount();
		int jobLogCount = xxlJobLogDao.triggerCountByHandleCode(-1);
		int jobLogSuccessCount = xxlJobLogDao.triggerCountByHandleCode(ReturnT.SUCCESS_CODE);

		// executor count
		Set<String> executerAddressSet = new HashSet<String>();
		List<XxlJobGroup> groupList = xxlJobGroupDao.findAll();

		if (CollectionUtils.isNotEmpty(groupList)) {
			for (XxlJobGroup group: groupList) {
				if (CollectionUtils.isNotEmpty(group.getRegistryList())) {
					executerAddressSet.addAll(group.getRegistryList());
				}
			}
		}

		int executorCount = executerAddressSet.size();

		Map<String, Object> dashboardMap = new HashMap<String, Object>();
		dashboardMap.put("jobInfoCount", jobInfoCount);
		dashboardMap.put("jobLogCount", jobLogCount);
		dashboardMap.put("jobLogSuccessCount", jobLogSuccessCount);
		dashboardMap.put("executorCount", executorCount);
		return dashboardMap;
	}

	@Override
	public ReturnT<Map<String, Object>> triggerChartDate() {
		Date from = DateUtils.addDays(new Date(), -30);
		Date to = new Date();

		List<String> triggerDayList = new ArrayList<String>();
		List<Integer> triggerDayCountSucList = new ArrayList<Integer>();
		List<Integer> triggerDayCountFailList = new ArrayList<Integer>();
		int triggerCountSucTotal = 0;
		int triggerCountFailTotal = 0;

		List<Map<String, Object>> triggerCountMapAll = xxlJobLogDao.triggerCountByDay(from, to, -1);
		List<Map<String, Object>> triggerCountMapSuc = xxlJobLogDao.triggerCountByDay(from, to, ReturnT.SUCCESS_CODE);
		if (CollectionUtils.isNotEmpty(triggerCountMapAll)) {
			for (Map<String, Object> item: triggerCountMapAll) {
				//换成大写,兼容Oracle
				//String day = String.valueOf(item.get("triggerDay"));
				String day = String.valueOf(item.get("TRIGGERDAY"));
				//int dayAllCount = Integer.valueOf(String.valueOf(item.get("triggerCount")));
				int dayAllCount = Integer.valueOf(String.valueOf(item.get("TRIGGERCOUNT")));
				int daySucCount = 0;
				int dayFailCount = dayAllCount - daySucCount;

				if (CollectionUtils.isNotEmpty(triggerCountMapSuc)) {
					for (Map<String, Object> sucItem: triggerCountMapSuc) {
						//String daySuc = String.valueOf(sucItem.get("triggerDay"));
						String daySuc = String.valueOf(sucItem.get("TRIGGERDAY"));
						if (day.equals(daySuc)) {
							//daySucCount = Integer.valueOf(String.valueOf(sucItem.get("triggerCount")));
							daySucCount = Integer.valueOf(String.valueOf(sucItem.get("TRIGGERCOUNT")));
							dayFailCount = dayAllCount - daySucCount;
						}
					}
				}

				triggerDayList.add(day);
				triggerDayCountSucList.add(daySucCount);
				triggerDayCountFailList.add(dayFailCount);
				triggerCountSucTotal += daySucCount;
				triggerCountFailTotal += dayFailCount;
			}
		} else {
			for (int i = 4; i > -1; i--) {
				triggerDayList.add(FastDateFormat.getInstance("yyyy-MM-dd").format(DateUtils.addDays(new Date(), -i)));
				triggerDayCountSucList.add(0);
				triggerDayCountFailList.add(0);
			}
		}

		Map<String, Object> result = new HashMap<String, Object>();
		result.put("triggerDayList", triggerDayList);
		result.put("triggerDayCountSucList", triggerDayCountSucList);
		result.put("triggerDayCountFailList", triggerDayCountFailList);
		result.put("triggerCountSucTotal", triggerCountSucTotal);
		result.put("triggerCountFailTotal", triggerCountFailTotal);
		return new ReturnT<Map<String, Object>>(result);
	}

	@Override
	public List<Object> executeTableKeySelect2(String vcName) {
		List<Object> list = xxlJobInfoDao.executeTableKeySelect2(vcName);
		return list;
	}

}
