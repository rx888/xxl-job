/**
 * ProcedureJobHandler.java .
 * <p>
 * Desc： .
 * Name： ProcedureJobHandler .
 * <p>
 * version    date         company      users       content
 * ———————————————————————————————————————————————————————------
 * Ver    2017年11月24日    Kaiyufound    shaozhimin
 * <p>
 * Copyright &copy; 2017 <a href="http://www.kaiyufound.com/">Kaiyu</a> All rights reserved .
 */
package com.xxl.job.core.handler.impl;

import com.kaiyufound.kres.exception.CommonException;
import com.kaiyufound.kres.utils.sql.CallProcedureUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.glue.GlueTypeEnum;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.log.XxlJobLogger;
import com.xxl.job.core.util.JacksonUtil;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.type.TypeReference;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 存储过程 JOBHandler .
 *
 * @author shaozhimin .
 * @version Ver 1.0.0 .
 * @see .
 * @since kaiyu-platform JDK 1.6.
 */
public class ProcedureJobHandler extends IJobHandler {

    private int jobId;
    private long glueUpdatetime;
    private String gluesource;
    private GlueTypeEnum glueType;
    private String procedureName;

    /**
     * PLACEHOLDER 存储过程占位符 .
     */
    private static final String PLACEHOLDER = "?";
    private static final String SPLITREGEX = ",";

    public static final String DELIM_START = "{";
    public static final String DELIM_END = "}";

    private static final String FIELD_INDEX = "index";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_PARAMTYPE = "paramType";
    private static final String FIELD_VALUETYPE = "valueType";
    private static final String FIELD_VALUE = "value";

    public ProcedureJobHandler(int jobId, long glueUpdatetime, String gluesource, GlueTypeEnum glueType) {
        this.jobId = jobId;
        this.glueUpdatetime = glueUpdatetime;
        this.gluesource = gluesource;
        this.glueType = glueType;
    }

    @Override
    public ReturnT<String> execute(String... params) throws Exception {
        // 判断存储过程名
        if(null == params || params.length == 0){
            return new ReturnT<String>(ReturnT.FAIL_CODE, "Procedure name is null");
        }
        // 因为存储过程是{CALL PROCEDURE(?, ?, ?)} 这种格式, 所以以','分割的数据散落在params数组不同下标
        String procName = "";
        for (int i=0; i< params.length; i++) {
            procName = procName + params[i];
            // } 结尾
            if (StringUtils.contains(params[i], DELIM_END)) {
               break;
            } else {
                procName = procName + SPLITREGEX;
            }
        }
        this.setProcedureName(procName);

        XxlJobLogger.log("----------- Procedure name :" + this.getProcedureName() + " -----------");
        XxlJobLogger.log("----------- Procedure source begin -----------");
        XxlJobLogger.log(this.getGluesource());
        XxlJobLogger.log("----------- Procedure source end -----------");
        XxlJobLogger.log("----------- Procedure version :" + this.getGlueUpdatetime() + " -----------");

        // 校验参数存储过程名与 参数source
        try {
            if (!checkSource()) {
                return new ReturnT<String>(ReturnT.FAIL_CODE, "Procedure parameter is failed");
            }
        } catch (Exception e) {
            XxlJobLogger.log("----------- Procedure error :" + e.getMessage() + " -----------");
            printException(e);
            return new ReturnT<String>(ReturnT.FAIL_CODE, "Procedure execute failed");
        }

        try{
            Map<String, Object> outValues = execute();
            // 输出执行结果
            printOutValues(outValues);
        } catch (Exception e){
            XxlJobLogger.log("----------- Procedure error :" + e.getMessage() + " -----------");
            printException(e);
            return new ReturnT<String>(ReturnT.FAIL_CODE, "Procedure execute failed");
        }
        return ReturnT.SUCCESS;
    }

    /**
     *
     * 校验存储过程名、及参数JSON数据 .
     *
     * @return
     */
    protected boolean checkSource() {
        int placeholderNum = StringUtils.countMatches(this.getProcedureName(), PLACEHOLDER);
        if(0 == placeholderNum){
            return true;
        }
        if(null == this.getGluesource() || "".equals(this.getGluesource())) {
            throw new CommonException("存储过程参数/名称为空!");
        }

        List<Map<String, Object>> list = null;

        try {
            list = JacksonUtil.toObject(this.getGluesource(), new TypeReference<List<Map<String, Object>>>() {});
        } catch (Exception e) {
            throw new CommonException("存储过程参数格式 校验失败!", e);
        }

        if (null == list || placeholderNum != list.size()) {
            throw new CommonException("存储过程定义与参数个数不一致!");
        }

        return true;
    }

    /**
     *
     * 执行存储过程 .
     *
     * @return 返回结果(参数名, 值) .
     * @throws Exception
     */
    protected Map<String, Object> execute() throws Exception {
        int placeholderNum = StringUtils.countMatches(this.getProcedureName(), PLACEHOLDER);
        if(0 == placeholderNum) {
            return CallProcedureUtil.callProcedure(this.getProcedureName(), null, null, null);
        }
        
        // 解析参数

        List<Map<String, Object>> list = JacksonUtil.toObject(this.getGluesource(), new TypeReference<List<Map<String, Object>>>() {});

        // 先按下标排序
        Collections.sort(list, new Comparator<Map<String, Object>>() {
            @Override
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                Object index1 = o1.get(FIELD_INDEX);
                Object index2 = o2.get(FIELD_INDEX);
                if (null == index1 || null == index2) {
                    return 0;
                }
                int diff = Integer.valueOf(index1.toString().trim()) - Integer.valueOf(index2.toString().trim());
                if (diff > 0) {
                    return 1;
                } else if (diff < 0) {
                    return -1;
                }
                return 0;
            }
        });

        List<Map<String, Integer>> parameterTypes = new ArrayList<Map<String, Integer>>(list.size());
        Map<Integer, Object> inParameter = new HashMap<Integer, Object>(list.size());
        Map<Integer, Integer> outParameter = new HashMap<Integer, Integer>(list.size());

        for(Map<String, Object> map : list) {
            Object index = map.get(FIELD_INDEX);
            Object name = map.get(FIELD_NAME);
            Object paramType = map.get(FIELD_PARAMTYPE);
            if (null == index || null == name || null == paramType) {
                // 参数未指定类型, 即使调用也会出错 (如果都是输入参数, 而且不指定具体的字段, 以下标为依据,则可以执行)
                XxlJobLogger.log("----------- Procedure 参数未指定下标、名称或类型(输入/输出) :" + map.toString() + " -----------");
                continue;
            }
            if (null == ParamTypes.getParamType(paramType.toString())) {
                XxlJobLogger.log("----------- Procedure 参数类型错误, 只能是(IN 或 OUT) :" + map.toString() + " -----------");
                continue;
            }
            // 值类型
            Object valueType = map.get(FIELD_VALUETYPE);
            if (null == valueType || null == ValueTypes.getValueType(valueType.toString())) {
                XxlJobLogger.log("----------- Procedure 参数值类型错误, 暂支持(CURSOR/VARCHAR/NUMERIC) :" + map.toString() + " -----------");
                continue;
            }
            if(ParamTypes.IN == ParamTypes.getParamType(paramType.toString())) {
                inParameter.put(Integer.valueOf(index.toString().trim()), map.get(FIELD_VALUE));
            } else {
                outParameter.put(Integer.valueOf(index.toString().trim()), ValueTypes.getValueType(valueType.toString()).code);
            }
            Map<String, Integer> parameterType = new HashMap<String, Integer>(1);
            parameterType.put(name.toString(), ValueTypes.getValueType(valueType.toString()).code);
            parameterTypes.add(parameterType);

        }
        // clazzMap 设置为空, 默认返回 Map结果集
        return CallProcedureUtil.callProcedure(this.getProcedureName(), parameterTypes, inParameter, outParameter, null);
    }

    protected void printOutValues(Map<String, Object> outValues) {
        if(null == outValues || outValues.isEmpty()) {
            return;
        }
        for (Map.Entry<String, Object> entry : outValues.entrySet()) {
            XxlJobLogger.log("----------- Procedure out key: " + entry.getKey() + "-----------");
            Object value = entry.getValue();
            if (null == value) {
                XxlJobLogger.log("----------- Procedure out data is empty -----------");
                continue;
            }
            if (value instanceof List) {
                List<Map<String, Object>> result = (List<Map<String, Object>>) value;
                XxlJobLogger.log("----------- Procedure data row count: " + result.size() + "-----------");
                for (int i = 0; i < result.size(); ++i) {
                    Map<String, Object> map = result.get(i);
                    if (null == map || map.isEmpty()) {
                        continue;
                    }

                    XxlJobLogger.log("----------- Procedure data row (" + i + ") " + "-----------");
                    XxlJobLogger.log(map.toString());
                }
            } else {
                XxlJobLogger.log("----------- Procedure out data: " + entry.getValue() + "-----------");
            }
        }
    }

    private void printException(Exception e) {
        StringWriter stringWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stringWriter, true));
        XxlJobLogger.log(stringWriter.toString());
    }

    /**
     * 参数类型 .
     */
    public enum ParamTypes {

        /**
         * 输入参数
         */
        IN("IN", "输入参数"),
        /**
         * 输出参数
         */
        OUT("OUT", "输出参数");

        /**
         * key value
         */
        protected String code;
        /**
         * desc
         */
        protected String name;

        private ParamTypes(String code, String name) {
            this.code = code;
            this.name = name;
        }

        @Override
        public String toString() {
            return this.code;
        }

        public String getCode() {
            return this.code;
        }

        public String getName() {
            return this.name;
        }

        public static String getName(String code) {
            ParamTypes[] types = ParamTypes.values();
            for (ParamTypes type : types) {
                if (type.code.equals(code)) {
                    return type.name;
                }
            }
            return null;
        }

        public static ParamTypes getParamType(String name) {
            try {
                return ParamTypes.valueOf(name);
            } catch (Exception e) {
                return null;
            }
        }
    }

    /**
     * 参数值类型 .
     */
    public enum ValueTypes {

        /**
         * 游标(Oracle)
         */
        CURSOR(-10, "游标结果集"),
        /**
         * 字符串
         */
        VARCHAR(12, "字符串"),
        /**
         * 数字
         */
        NUMERIC(2, "数字");

        /**
         * key value
         */
        protected int code;
        /**
         * desc
         */
        private String name;

        private ValueTypes(int code, String name) {
            this.code = code;
            this.name = name;
        }

        @Override
        public String toString() {
            return String.valueOf(this.code);
        }

        public int getCode() {
            return this.code;
        }

        public String getName() {
            return this.name;
        }

        public static String getName(int code) {
            ValueTypes[] types = ValueTypes.values();
            for (ValueTypes type : types) {
                if (type.code == code) {
                    return type.name;
                }
            }
            return null;
        }

        public static ValueTypes getValueType(String name) {
            try {
                return ValueTypes.valueOf(name);
            } catch (Exception e) {
                return null;
            }
        }
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public long getGlueUpdatetime() {
        return glueUpdatetime;
    }

    public void setGlueUpdatetime(long glueUpdatetime) {
        this.glueUpdatetime = glueUpdatetime;
    }

    public String getGluesource() {
        return gluesource;
    }

    public void setGluesource(String gluesource) {
        this.gluesource = gluesource;
    }

    public GlueTypeEnum getGlueType() {
        return glueType;
    }

    public void setGlueType(GlueTypeEnum glueType) {
        this.glueType = glueType;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }
}
