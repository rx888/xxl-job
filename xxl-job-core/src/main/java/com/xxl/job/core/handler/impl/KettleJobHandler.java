/**
 * KettleJobHandler.java .
 * <p>
 * Desc： .
 * Name： KettleJobHandler .
 * <p>
 * version    date         company      users       content
 * ———————————————————————————————————————————————————————------
 * Ver    2018年11月20日    Kaiyufound    shaozhimin
 * <p>
 * Copyright &copy; 2017 <a href="http://www.kaiyufound.com/">Kaiyu</a> All rights reserved .
 */
package com.xxl.job.core.handler.impl;

import com.kaiyufound.kres.exception.CommonException;
import com.kaiyufound.kres.utils.pentaho.KettleUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.glue.GlueTypeEnum;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.log.XxlJobLogger;
import com.xxl.job.core.util.JacksonUtil;
import org.codehaus.jackson.type.TypeReference;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

/**
 * Kettle JOBHandler .
 *
 * @author shaozhimin .
 * @version Ver 1.0.0 .
 * @see .
 * @since kaiyu-platform JDK 1.6.
 */
public class KettleJobHandler extends IJobHandler {

	private int jobId;
	private long glueUpdatetime;
	private String gluesource;
	private GlueTypeEnum glueType;
	private String kettleName;

	public KettleJobHandler(int jobId, long glueUpdatetime, String gluesource, GlueTypeEnum glueType) {
		this.jobId = jobId;
		this.glueUpdatetime = glueUpdatetime;
		this.gluesource = gluesource;
		this.glueType = glueType;
	}

	@Override
	public ReturnT<String> execute(String... params) throws Exception {

        // 判断 kettle 名
        if (null == params || params.length == 0) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, "Kettle name/path is null");
        }
        this.setKettleName(params[0]);

        XxlJobLogger.log("----------- Kettle name/path :" + this.getKettleName() + " -----------");
        XxlJobLogger.log("----------- Kettle source begin -----------");
        XxlJobLogger.log(this.getGluesource());
        XxlJobLogger.log("----------- Kettle source end -----------");
        XxlJobLogger.log("----------- Kettle version :" + this.getGlueUpdatetime() + " -----------");

        // 校验参数source
        try {
            if (!checkSource()) {
                return new ReturnT<String>(ReturnT.FAIL_CODE, "Kettle parameter is failed");
            }
        } catch (Exception e) {
            XxlJobLogger.log("----------- Kettle error :" + e.getMessage() + " -----------");
            printException(e);
            return new ReturnT<String>(ReturnT.FAIL_CODE, "Kettle execute failed");
        }

        try {
            execute();
        } catch (Exception e) {
            XxlJobLogger.log("----------- Kettle error :" + e.getMessage() + " -----------");
            printException(e);
            return new ReturnT<String>(ReturnT.FAIL_CODE, "Kettle execute failed");
        }

        return ReturnT.SUCCESS;
	}

	/**
	 * 校验kettle名、及参数JSON数据 .
	 *
	 * @return
	 */
	protected boolean checkSource() {

		if(null == this.gluesource || "".equals(this.gluesource.trim())) {
			return true;
		}

		try {
			JacksonUtil.toObject(this.getGluesource(), new TypeReference<List<Map<String, Object>>>() {});
		} catch (Exception e) {
			throw new CommonException("Kettle参数格式 校验失败!", e);
		}
		return true;
	}

	/**
	 * 执行Kettle文件 .
	 *
	 * @return 返回结果(参数名, 值) .
	 * @throws Exception
	 */
	protected void execute() throws Exception {
		
		if(null == this.gluesource || "".equals(this.gluesource.trim())) {
			KettleUtil.runJob(null, this.getKettleName());
			return;
		}
		/***
		 * 2019-02-19
		 *上一版本直接接收单一的map，为了，kettle和存储参数共用，故将kettle参数传值类型改为数组
		 */
		List<Map<String, Object>> kettleParamList = JacksonUtil.toObject(this.getGluesource(), new TypeReference<List<Map<String, Object>>>() {});
		KettleUtil.runJob(kettleParamList, this.getKettleName());
	}

    private void printException(Exception e) {
        StringWriter stringWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stringWriter, true));
        XxlJobLogger.log(stringWriter.toString());
    }

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	public long getGlueUpdatetime() {
		return glueUpdatetime;
	}

	public void setGlueUpdatetime(long glueUpdatetime) {
		this.glueUpdatetime = glueUpdatetime;
	}

	public String getGluesource() {
		return gluesource;
	}

	public void setGluesource(String gluesource) {
		this.gluesource = gluesource;
	}

	public GlueTypeEnum getGlueType() {
		return glueType;
	}

	public void setGlueType(GlueTypeEnum glueType) {
		this.glueType = glueType;
	}

	public String getKettleName() {
		return kettleName;
	}

	public void setKettleName(String kettleName) {
		this.kettleName = kettleName;
	}
}
