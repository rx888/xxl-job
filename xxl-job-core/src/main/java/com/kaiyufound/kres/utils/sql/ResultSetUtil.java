/**
 * ResultSetUtil.java .
 * <p>
 * Desc： .
 * Name： ResultSetUtil .
 * <p>
 * version    date         company      users       content
 * ———————————————————————————————————————————————————————------
 * Ver    2017年11月24日    Kaiyufound    shaozhimin
 * <p>
 * Copyright &copy; 2017 <a href="http://www.kaiyufound.com/">Kaiyu</a> All rights reserved .
 */
package com.kaiyufound.kres.utils.sql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 存储过程工具 .
 *
 * @author shaozhimin .
 * @version Ver 1.0.0 .
 * @dateTime 2018年11月0日 上午12:45:38 .
 * @see .
 * @since kaiyu-platform JDK 1.6.
 */
public final class ResultSetUtil {

    private ResultSetUtil() {
        
    }

    /**
     * LOGGER 日志 .
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ResultSetUtil.class);

    /**
     * 结果集转换-转换成Map .
     *
     * @param rs 结果集 .
     * @return List<Map < String ,   O bject>> .
     * @throws SQLException .
     * @author shaozhimin .
     */
    public static List<Map<String, Object>> rowMapperMap(ResultSet rs) throws SQLException {
        return rowMapper(rs, getColumnMapRowMapper());
    }

    /**
     * 结果集转换-根据RowMapper .
     *
     * @param rs        结果集 .
     * @param rowMapper 行记录转换接口 .
     * @return List<T> .
     * @throws SQLException .
     * @author shaozhimin .
     */
    public static <T> List<T> rowMapper(ResultSet rs, RowMapper<T> rowMapper) throws SQLException {
        List<T> results = new ArrayList<T>();
        ResultSet rsToUse = rs;
        int rowNum = 0;
        while (rsToUse.next()) {
            results.add(rowMapper.mapRow(rsToUse, rowNum++));
        }
        return results;
    }

    /**
     *
     * 返回默认的Map映射 .
     *
     * @return
     */
    public static RowMapper<Map<String, Object>> getColumnMapRowMapper() {
        return new ColumnMapRowMapper();
    }
}
