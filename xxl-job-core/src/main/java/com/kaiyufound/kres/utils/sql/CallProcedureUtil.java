/**
 * CallProcedureUtil.java .
 * <p>
 * Desc： .
 * Name： CallProcedureUtil .
 * <p>
 * version    date         company      users       content
 * ———————————————————————————————————————————————————————------
 * Ver    2017年11月24日    Kaiyufound    shaozhimin
 * <p>
 * Copyright &copy; 2017 <a href="http://www.kaiyufound.com/">Kaiyu</a> All rights reserved .
 */
package com.kaiyufound.kres.utils.sql;


import com.kaiyufound.kres.exception.CommonException;
import oracle.jdbc.OracleTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 存储过程工具 .
 *
 * @author shaozhimin .
 * @version Ver 1.0.0 .
 * @dateTime 2018年11月0日 上午12:45:38 .
 * @see .
 * @since kaiyu-platform JDK 1.6.
 */
public final class CallProcedureUtil {

    /**
     * LOGGER 日志 .
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CallProcedureUtil.class);

    /**
     * 使用 JdbcTemplate 做查询、SQL执行 (后续可考虑放入XxlJobExecutor).
     */
    private static JdbcTemplate jdbcTemplate;

    private CallProcedureUtil() {
        
    }

    public JdbcTemplate getJdbcTemplate() {
        return CallProcedureUtil.jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        CallProcedureUtil.jdbcTemplate = jdbcTemplate;
    }

    /**
     *
     * @param excutorSql 存储过程SQL: {CALL PROCEDURE_NAME(?,?,?)} .
     * @param parameterTypes 输入/输出参数 参数名/类型 .
     * @param inParameter 输入参数 下标/值 .
     * @param outParameter 输出参数 下标/类型 .
     * @param clazzMap 转换类型/默认返回Map 结果集 .
     * @return 返回结果, 以Map方式<参数名, 返回值> .
     * @throws Exception
     */
    public static Map<String, Object> callProcedure(final String excutorSql, final List<Map<String, Integer>> parameterTypes, final Map<Integer, Object> inParameter, final Map<Integer, Integer> outParameter, final Map<String, Class<?>> clazzMap) throws Exception {

        if (null == CallProcedureUtil.jdbcTemplate) {
            throw new CommonException("CallProcedureUtil.jdbcTemplate  is null!");
        }
        
        // paramsTpyes 主要设置 参数名的类型, 不是占位符下标, 这样可以支持使用参数名获取返回值
        List<SqlParameter> sqlParameters = new ArrayList<SqlParameter>();
        for (int i = 0; i < parameterTypes.size(); i++) {
            int index = i + 1;
            Map.Entry<String, Integer> paramsTpyeEntry = null;
            for (Map.Entry<String, Integer> entry : parameterTypes.get(i).entrySet()) {
                // 只有一个元素
                paramsTpyeEntry = entry;
                break;
            }
            // 判断输入 or 输出
            if (inParameter.containsKey(index)) {
                sqlParameters.add(new SqlParameter(paramsTpyeEntry.getKey(), paramsTpyeEntry.getValue()));
            }
            if (outParameter.containsKey(index)) {
                if (OracleTypes.CURSOR == paramsTpyeEntry.getValue()) {
                    if (null == clazzMap || null == clazzMap.get(paramsTpyeEntry.getKey()) || Map.class == clazzMap.get(paramsTpyeEntry.getKey()) || clazzMap.get(paramsTpyeEntry.getKey()).isAssignableFrom(Map.class)) {
                        sqlParameters.add(new SqlReturnResultSet(paramsTpyeEntry.getKey(), ResultSetUtil.getColumnMapRowMapper()));
                    }
                } else {
                    sqlParameters.add(new SqlOutParameter(paramsTpyeEntry.getKey(), paramsTpyeEntry.getValue()));
                }
            }
        }

        Map<String, Object> outValues = CallProcedureUtil.jdbcTemplate.call(new CallableStatementCreator() {
            @Override
            public CallableStatement createCallableStatement(Connection conn)
                    throws SQLException {
                // 获取CallableStatement
                CallableStatement cstmt = conn.prepareCall(excutorSql);
                // 设置输入参数
                for (Map.Entry<Integer, Object> entry : inParameter.entrySet()) {
                    cstmt.setObject(entry.getKey(), entry.getValue());
                }
                // 注册输出参数
                for (Map.Entry<Integer, Integer> entry : outParameter.entrySet()) {
                    cstmt.registerOutParameter(entry.getKey(), entry.getValue());
                }
                return cstmt;
            }
        }, sqlParameters);
        
        return outValues;
    }

    /**
     *
     * 执行存储过程 .
     *
     * @param excutorSql 存储过程SQL: {CALL PROCEDURE_NAME(?,?,?)} .
     * @param inParameter 输入参数 下标/值 .
     * @param outParameter 输出参数 下标/类型 .
     * @param clazzMap 转换类型/默认返回Map 结果集 .
     * @return 返回结果,以Map方式<参数下标, 返回值> .
     * @throws Exception
     */
    public static Map<String, Object> callProcedure(final String excutorSql, final Map<Integer, Object> inParameter, final Map<Integer, Integer> outParameter, final Map<Integer, Class<?>> clazzMap) throws Exception {

        if(null == CallProcedureUtil.jdbcTemplate){
            throw new CommonException("CallProcedureUtil.jdbcTemplate  is null!");
        }

        Map<String, Object> outValues = CallProcedureUtil.jdbcTemplate.execute(new CallableStatementCreator() {
            @Override
            public CallableStatement createCallableStatement(Connection conn)
                    throws SQLException {
                CallableStatement cstmt = conn.prepareCall(excutorSql);
                //设置输入参数
                if (null != inParameter){
                    for (Map.Entry<Integer, Object> entry : inParameter.entrySet()) {
                        cstmt.setObject(entry.getKey(), entry.getValue());
                    }
                }
                //设置输出参数
                if (null != outParameter){
                    for (Map.Entry<Integer, Integer> entry : outParameter.entrySet()) {
                        cstmt.registerOutParameter(entry.getKey(), entry.getValue());
                    }
                }
                return cstmt;
            }
        }, new CallableStatementCallback<Map<String, Object>>() {
            @Override
            public Map<String, Object> doInCallableStatement(CallableStatement cstmt)
                    throws SQLException, DataAccessException {
                //执行
                cstmt.execute();
                Map<String, Object> outValues = new HashMap<String, Object>();
                if (null == outParameter){
                    return outValues;
                }
                for (Map.Entry<Integer, Integer> entry : outParameter.entrySet()) {
                    String entryKey = String.valueOf(entry.getKey());
                    //游标结果集
                    if (OracleTypes.CURSOR == entry.getValue()) {
                        if (null == clazzMap || null == clazzMap.get(entry.getKey()) || Map.class == clazzMap.get(entry.getKey()) || clazzMap.get(entry.getKey()).isAssignableFrom(Map.class)) {
                            //默认 Map
                            outValues.put(entryKey, ResultSetUtil.rowMapperMap((ResultSet) cstmt.getObject(entry.getKey())));
                        }
                    }
                    //String 类型
                    if (OracleTypes.VARCHAR == entry.getValue() || Types.VARCHAR == entry.getValue()) {
                        outValues.put(entryKey, cstmt.getString(entry.getKey()));
                    }
                    //int 类型
                    if (OracleTypes.INTEGER == entry.getValue() || Types.INTEGER == entry.getValue()) {
                        outValues.put(entryKey, cstmt.getInt(entry.getKey()));
                    }
                    //其他类型自行添加
                }
                return outValues;
            }
        });
        return outValues;
    }

}
