/**
 * KettleUtil.java .
 * <p>
 * Desc： .
 * Name： KettleUtil .
 * <p>
 * version    date         company      users       content
 * ———————————————————————————————————————————————————————------
 * Ver    2018年11月20日    Kaiyufound    shaozhimin
 * <p>
 * Copyright &copy; 2017 <a href="http://www.kaiyufound.com/">Kaiyu</a> All rights reserved .
 */
package com.kaiyufound.kres.utils.pentaho;

import com.kaiyufound.kres.config.setting.ConfigurationSetting;
import com.kaiyufound.kres.exception.CommonException;
import com.kaiyufound.kres.utils.spring.SpringContextUtil;
import com.xxl.job.core.log.XxlJobLogger;
import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.exception.KettleXMLException;
import org.pentaho.di.core.parameters.UnknownParamException;
import org.pentaho.di.job.Job;
import org.pentaho.di.job.JobMeta;
import org.pentaho.di.repository.ObjectId;
import org.pentaho.di.repository.filerep.KettleFileRepository;
import org.pentaho.di.repository.filerep.KettleFileRepositoryMeta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public final class KettleUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(KettleUtil.class);
	// 文件形式的资源库
	private static KettleFileRepository kettleRep = null;
	// 资源库元对象
	private KettleFileRepositoryMeta fileRepositoryMeta = null;

	public KettleFileRepositoryMeta getFileRepositoryMeta() {
		return fileRepositoryMeta;
	}

	public void setFileRepositoryMeta(KettleFileRepositoryMeta fileRepositoryMeta) {
		this.fileRepositoryMeta = fileRepositoryMeta;
	}

	private KettleUtil() {

	}

	public void init() throws Exception {
		try {
			String user_dir = SpringContextUtil.getProperty("user.dir");
			String kettleHome = SpringContextUtil.getApplicationContext().getResource("/WEB-INF").getFile().getPath();
			System.setProperty("user.dir", kettleHome);
			System.setProperty("KETTLE_HOME", kettleHome);
			// 初始化
			KettleEnvironment.init();
			System.setProperty("user.dir", user_dir);
			/***
			 * 2019-02-18 添加
			 * rx
			 * 原本是在这里直接读取配置文件的资源库配置信息，
			 * 但是在启动的时候，报错，因此，资源库配置信息写在spring配置文件注入到该对象
			 */
			// 文件形式的资源库
			kettleRep = new KettleFileRepository();
			kettleRep.setRepositoryMeta(fileRepositoryMeta);
			kettleRep.init(fileRepositoryMeta);
		}catch (Exception e) {
			LOGGER.error("KettleUtil init KettleEnvironment error", e);
			throw new CommonException("KettleUtil init KettleEnvironment error", e);
		}
	}

	public void destroy() {
		KettleEnvironment.shutdown();
	}

	public static void runJob(String jobPath) throws Exception {
		runJob(null, jobPath);
	}

	/**
	 *
	 * 执行Kettle文件 .
	 *
	 * @param kettleParamList 参数 .
	 * @param jobPath 文件路径 .
	 * @throws Exception .
	 */
	public static void runJob(List<Map<String, Object>> kettleParamList, String jobPath) throws Exception {
		if(null == jobPath || "".equals(jobPath)) {
			throw new CommonException("KettleUtil runJob jobPath is null");
		}
		/***
		 * 2019-02-18 修改
		 * 资源库不需要文件的路径只需要执行文件的名字
		 * 因此做一个判断如果包含文件后缀则执行全量文件路径执行kettle文件
		 * 如果没有则说明是资源库文件，则用资源库去实例化
		 * 临时修改
		 * 后续大佬们要把这个功能分离，执行kettle类型，和调用路径，执行参数等等都需要重新考量，先将就用用
		 */
		JobMeta jobMeta = null;
		Job job = null;
		try {
			//判断文件路径是否包含后缀.kjb
			//如果包含则表示为全量路径，反之则说明执行资源库文件，（资源库文件只用需要文件名称）
			if(jobPath.contains(".kjb")){
				LOGGER.info("KettleUtil runJob 执行文件路径：", jobPath);
				jobMeta = new JobMeta(jobPath, null);
				job = new Job(null, jobMeta);
			}else{
				ObjectId objectId = kettleRep.getJobId(jobPath, null);
				jobMeta = kettleRep.loadJob(objectId, null);
				job = new Job(kettleRep, jobMeta);
				LOGGER.info("KettleUtil runJob 执行文件：", objectId);
			}
			/***
			 * 2019-02-19 rx
			 *   这个是存储过程参数类型，kettle就按照这个类型来
			 *[

				 {
					 "index":1,
					 "name":"param2",
					 "paramType":"IN",
					 "valueType":"VARCHAR",
					 "value":"123"
				 },
				 {
					 "index":2,
					 "name":"param1",
					 "paramType":"IN",
					 "valueType":"VARCHAR",
					 "value":"123"
				 }
			 ]
			 * 为了数据格式的统一，将统一用name的值做key，value做值 kettle的值
			 */
			if(null != kettleParamList && kettleParamList.size() >0) {
				for(Map<String, Object> mapObj : kettleParamList){
					if(null != mapObj && !mapObj.isEmpty()){
						XxlJobLogger.log("KettleUtil runJob 接收参数 ： " + mapObj.get("name").toString()+":" + mapObj.get("value").toString());
						if(null == mapObj.get("name") || "".equals(mapObj.get("name"))){
							throw new CommonException("KettleUtil runJob error, \n" + "参数key值为name的未取到对应值，请查看参数输入是否正确");
						}else{
							jobMeta.setParameterValue(mapObj.get("name").toString(), mapObj.get("value").toString());
						}
					}
				}
			}
			job.start();
			job.waitUntilFinished();

			if (job.getErrors() > 0) {
				LOGGER.error("KettleUtil runJob error, {}", job.getErrors());
				throw new CommonException("KettleUtil runJob error, \n" + job.getErrors());
			}

		} catch (KettleXMLException e) {
			throw new CommonException("KettleUtil runJob create JobMeta error");
		} catch (UnknownParamException e) {
			throw new CommonException("KettleUtil runJob setParameterValue error");
		} catch (Exception e) {
			if (e instanceof CommonException) {
				throw e;
			} else {
				throw new CommonException("KettleUtil runJob error", e);
			}
		}

	}

}
