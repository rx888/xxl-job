-------------------------------------------
-- Export file for user JOB         --
-- Created by itbaymax on 2017/10/11, 11:04:38--
-------------------------------------------

set define off
spool JOB_QRTZ.log

prompt
prompt Creating table JOB_QRTZ_BLOB_TRIGGERS
prompt =====================================
prompt
create table JOB_QRTZ_BLOB_TRIGGERS
(
  sched_name    VARCHAR2(120) not null,
  trigger_name  VARCHAR2(200) not null,
  trigger_group VARCHAR2(200) not null,
  blob_data     BLOB
)
;
alter table JOB_QRTZ_BLOB_TRIGGERS
  add constraint PRIMARYKEY4 primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);

prompt
prompt Creating table JOB_QRTZ_CALENDARS
prompt =================================
prompt
create table JOB_QRTZ_CALENDARS
(
  sched_name    VARCHAR2(120) not null,
  calendar_name VARCHAR2(200) not null,
  calendar      BLOB
)
;
alter table JOB_QRTZ_CALENDARS
  add constraint PRIMARYKEY5 primary key (SCHED_NAME, CALENDAR_NAME);

prompt
prompt Creating table JOB_QRTZ_CRON_TRIGGERS
prompt =====================================
prompt
create table JOB_QRTZ_CRON_TRIGGERS
(
  sched_name      VARCHAR2(120) not null,
  trigger_name    VARCHAR2(200) not null,
  trigger_group   VARCHAR2(200) not null,
  cron_expression VARCHAR2(200) not null,
  time_zone_id    VARCHAR2(80)
)
;
alter table JOB_QRTZ_CRON_TRIGGERS
  add constraint PRIMARYKEY6 primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);

prompt
prompt Creating table JOB_QRTZ_FIRED_TRIGGERS
prompt ======================================
prompt
create table JOB_QRTZ_FIRED_TRIGGERS
(
  sched_name        VARCHAR2(120) not null,
  entry_id          VARCHAR2(95) not null,
  trigger_name      VARCHAR2(200) not null,
  trigger_group     VARCHAR2(200) not null,
  instance_name     VARCHAR2(200) not null,
  fired_time        NUMBER(13),
  sched_time        NUMBER(13) not null,
  priority          NUMBER(11) not null,
  state             VARCHAR2(16) not null,
  job_name          VARCHAR2(200),
  job_group         VARCHAR2(200),
  is_nonconcurrent  VARCHAR2(200),
  requests_recovery VARCHAR2(1)
)
;
alter table JOB_QRTZ_FIRED_TRIGGERS
  add constraint PRIMAYKEY7 primary key (SCHED_NAME, ENTRY_ID);

prompt
prompt Creating table JOB_QRTZ_JOB_DETAILS
prompt ===================================
prompt
create table JOB_QRTZ_JOB_DETAILS
(
  sched_name        VARCHAR2(120) not null,
  job_name          VARCHAR2(200) not null,
  job_group         VARCHAR2(200) not null,
  description       VARCHAR2(250),
  job_class_name    VARCHAR2(250) not null,
  is_durable        VARCHAR2(1) not null,
  is_nonconcurrent  VARCHAR2(1) not null,
  is_update_data    VARCHAR2(1) not null,
  requests_recovery VARCHAR2(1) not null,
  job_data          BLOB
)
;
alter table JOB_QRTZ_JOB_DETAILS
  add constraint PRIMARYKEY8 primary key (SCHED_NAME, JOB_NAME, JOB_GROUP);

prompt
prompt Creating table JOB_QRTZ_LOCKS
prompt =============================
prompt
create table JOB_QRTZ_LOCKS
(
  sched_name VARCHAR2(120) not null,
  lock_name  VARCHAR2(40) not null
)
;
alter table JOB_QRTZ_LOCKS
  add constraint PRIMARYKEY9 primary key (SCHED_NAME, LOCK_NAME);

prompt
prompt Creating table JOB_QRTZ_PAUSED_TRIGGER_GRPS
prompt ===========================================
prompt
create table JOB_QRTZ_PAUSED_TRIGGER_GRPS
(
  sched_name    VARCHAR2(120) not null,
  trigger_group VARCHAR2(200) not null
)
;
alter table JOB_QRTZ_PAUSED_TRIGGER_GRPS
  add constraint PRIMARYKEY11 primary key (SCHED_NAME, TRIGGER_GROUP);

prompt
prompt Creating table JOB_QRTZ_SCHEDULER_STATE
prompt =======================================
prompt
create table JOB_QRTZ_SCHEDULER_STATE
(
  sched_name        VARCHAR2(120) not null,
  instance_name     VARCHAR2(120) not null,
  last_checkin_time NUMBER(13) not null,
  checkin_interval  NUMBER(13) not null
)
;
alter table JOB_QRTZ_SCHEDULER_STATE
  add constraint PRIMARYKEY10 primary key (SCHED_NAME, INSTANCE_NAME);

prompt
prompt Creating table JOB_QRTZ_SIMPLE_TRIGGERS
prompt =======================================
prompt
create table JOB_QRTZ_SIMPLE_TRIGGERS
(
  sched_name      VARCHAR2(120) not null,
  trigger_name    VARCHAR2(200) not null,
  trigger_group   VARCHAR2(200) not null,
  repeat_count    NUMBER(7) not null,
  repeat_interval NUMBER(12) not null,
  times_triggered NUMBER(10) not null
)
;
alter table JOB_QRTZ_SIMPLE_TRIGGERS
  add constraint PRIMARYKEY primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);

prompt
prompt Creating table JOB_QRTZ_SIMPROP_TRIGGERS
prompt ========================================
prompt
create table JOB_QRTZ_SIMPROP_TRIGGERS
(
  sched_name    VARCHAR2(120) not null,
  trigger_name  VARCHAR2(200) not null,
  trigger_group VARCHAR2(200) not null,
  str_prop_1    VARCHAR2(512),
  str_prop_2    VARCHAR2(512),
  str_prop_3    VARCHAR2(512),
  int_prop_1    NUMBER(11),
  int_prop_2    NUMBER(11),
  long_prop_1   NUMBER(20),
  long_prop_2   NUMBER(20),
  dec_prop_1    NUMBER(13,4),
  dec_prop_2    NUMBER(13,4),
  bool_prop_1   VARCHAR2(1),
  bool_prop_2   VARCHAR2(1)
)
;
alter table JOB_QRTZ_SIMPROP_TRIGGERS
  add constraint PRIMARYKEY2 primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);

prompt
prompt Creating table JOB_QRTZ_TRIGGERS
prompt ================================
prompt
create table JOB_QRTZ_TRIGGERS
(
  sched_name     VARCHAR2(120) not null,
  trigger_name   VARCHAR2(200) not null,
  trigger_group  VARCHAR2(200) not null,
  job_name       VARCHAR2(200) not null,
  job_group      VARCHAR2(200) not null,
  description    VARCHAR2(250),
  next_fire_time NUMBER(13),
  prev_fire_time NUMBER(13),
  priority       NUMBER(11),
  trigger_state  VARCHAR2(16) not null,
  trigger_type   VARCHAR2(8) not null,
  start_time     NUMBER(13),
  end_time       NUMBER(13),
  calendar_name  VARCHAR2(200),
  misfire_instr  NUMBER(2),
  job_data       BLOB
)
;
alter table JOB_QRTZ_TRIGGERS
  add constraint PRIMARYKEY3 primary key (SCHED_NAME, TRIGGER_GROUP, TRIGGER_NAME);

prompt
prompt Creating table JOB_QRTZ_TRIGGER_GROUP
prompt =====================================
prompt
create table JOB_QRTZ_TRIGGER_GROUP
(
  id           NUMBER(11) not null,
  app_name     VARCHAR2(200) not null,
  title        VARCHAR2(200) not null,
  address_type NUMBER(4) not null,
  address_list VARCHAR2(200),
  group_order  NUMBER(4)
)
;
alter table JOB_QRTZ_TRIGGER_GROUP
  add primary key (ID);

prompt
prompt Creating table JOB_QRTZ_TRIGGER_INFO
prompt ====================================
prompt
create table JOB_QRTZ_TRIGGER_INFO
(
  id                      NUMBER(11) not null,
  job_group               NUMBER(11),
  job_cron                VARCHAR2(128),
  job_desc                VARCHAR2(255),
  add_time                DATE,
  update_time             DATE,
  author                  VARCHAR2(64),
  alarm_email             VARCHAR2(255),
  executor_route_strategy VARCHAR2(50),
  executor_handler        VARCHAR2(255),
  executor_param          VARCHAR2(255),
  executor_block_strategy VARCHAR2(50),
  executor_fail_strategy  VARCHAR2(50),
  glue_type               VARCHAR2(50),
  glue_source             CLOB,
  glue_remark             VARCHAR2(128),
  glue_updatetime         DATE,
  child_jobkey            VARCHAR2(255)
)
;
alter table JOB_QRTZ_TRIGGER_INFO
  add primary key (ID);

prompt
prompt Creating table JOB_QRTZ_TRIGGER_LOG
prompt ===================================
prompt
create table JOB_QRTZ_TRIGGER_LOG
(
  id               NUMBER(11) not null,
  job_group        NUMBER(11) not null,
  job_id           NUMBER(11) not null,
  glue_type        VARCHAR2(50),
  executor_address VARCHAR2(255),
  executor_handler VARCHAR2(255),
  executor_param   VARCHAR2(255),
  trigger_time     DATE,
  trigger_code     VARCHAR2(255),
  trigger_msg      VARCHAR2(2048),
  handle_time      DATE,
  handle_code      VARCHAR2(255),
  handle_msg       VARCHAR2(2048)
)
;
alter table JOB_QRTZ_TRIGGER_LOG
  add primary key (ID);

prompt
prompt Creating table JOB_QRTZ_TRIGGER_LOGGLUE
prompt =======================================
prompt
create table JOB_QRTZ_TRIGGER_LOGGLUE
(
  id          NUMBER(11) not null,
  job_id      NUMBER(11) not null,
  glue_type   VARCHAR2(500),
  glue_source CLOB,
  glue_remark VARCHAR2(128),
  add_time    DATE,
  update_time DATE
)
;
alter table JOB_QRTZ_TRIGGER_LOGGLUE
  add primary key (ID);

prompt
prompt Creating table JOB_QRTZ_TRIGGER_REGISTRY
prompt ========================================
prompt
create table JOB_QRTZ_TRIGGER_REGISTRY
(
  id             NUMBER(11) not null,
  registry_group VARCHAR2(255) not null,
  registry_key   VARCHAR2(255) not null,
  registry_value VARCHAR2(255) not null,
  update_time    DATE
)
;
alter table JOB_QRTZ_TRIGGER_REGISTRY
  add primary key (ID);

prompt
prompt Creating sequence JOB_KEY_SEQUENCE
prompt ==================================
prompt
create sequence JOB_KEY_SEQUENCE
minvalue 1
maxvalue 999999999
start with 1
increment by 1
cache 20;


spool off
